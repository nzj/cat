//Powered By zsCat, Since 2016 - 2020

package com.zs.pig.blog.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.blog.api.model.BlogType;


/**
 * 
 * @author zsCat 2016-6-14 13:56:36
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的blog
 */
public interface BlogTypeMapper extends Mapper<BlogType>{

}
