package com.zsCat.web.test.controller;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.zsCat.common.utils.PageUtil;
import com.zsCat.web.test.model.TestUser;
import com.zsCat.web.test.service.TestUserService;
	/**
	 * 
	 * @author zsCat 2016-11-7 19:44:30
	 * @Email: 951449465@qq.com
	 * @version 4.0v
	 *	test管理
	 */
@Controller
@RequestMapping("testUser")
public class TestUserController {

	@Resource
	private TestUserService TestUserService;
	
	@RequestMapping
	public String toTestUser(Model model){
		return "test/testUser/testUser";
	}
	
	@RequestMapping(value = "add")
	public  String add(@ModelAttribute TestUser TestUser) {
		return "test/testUser/testUser-save";
	}
	/**
	 * 添加或更新区域
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> save(@ModelAttribute TestUser TestUser) {
		Map<String, Object> msg = new HashMap<String, Object>();
		 TestUserService.saveTestUser(TestUser);
		return msg;
	}
	
	/**
	 * 删除字典
	* @param id
	* @return
	 */
	@RequestMapping(value="delete")
	public  String del(@ModelAttribute TestUser TestUser){
		 TestUserService.deleteTestUser(TestUser);
		return "redirect:/testUser/list";
	}
	
	/**
	 * 分页显示字典table
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "list")
	public String list(HttpServletRequest req,@RequestParam(required=false , value="page" ,defaultValue="1")int pageNum,
			@RequestParam(required=false , value="pageSize" ,defaultValue="15")int pageSize,@ModelAttribute TestUser TestUser, Model model) {
		PageInfo<TestUser> page = TestUserService.selectPage(pageNum, pageSize, TestUser);
		StringBuffer param=new StringBuffer(); // 查询参数
		model.addAttribute("page", page);
		model.addAttribute("pageCode",PageUtil.genMeiZiPagination(req.getContextPath()+"/testUser/list", TestUserService.selectCount(TestUser), pageNum, pageSize, param.toString()));
		return "test/testUser/testUser-list";
	}
	
	@RequestMapping(value="edit")
	public String showLayer(Long id, Model model){
		TestUser testUser = TestUserService.selectByPrimaryKey(id);
		model.addAttribute("item", testUser);
		return "test/testUser/testUser-save";
	}
	
}
