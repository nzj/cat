//Powered By if, Since 2014 - 2020

package com.zsCat.web.sys.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import com.zsCat.common.base.ServiceMybatis;
import com.zsCat.web.sys.mapper.SysAreaMapper;
import com.zsCat.web.sys.mapper.SysDictMapper;
import com.zsCat.web.sys.model.SysArea;
import com.zsCat.web.sys.model.SysDict;
import com.zsCat.web.sys.model.SysOffice;
import com.zsCat.web.sys.model.SysRole;

/**
 * 
 * @author
 */

@Service("sysDictService")
@CacheConfig(cacheNames = "sysDict_cache")
public class SysDictService extends ServiceMybatis<SysDict> {

	@Resource
	private SysDictMapper sysDictMapper;

	@Resource
	private SysAreaMapper sysAreaMapper;

	/**
	 * 保存或更新
	 * 
	 * @param sysDict
	 * @return
	 */
	@CacheEvict(allEntries = true)
	public int saveSysdict(SysDict sysDict) {
		return this.save(sysDict);
	}

	/**
	 * 删除
	* @param sysDict
	* @return
	 */
	@CacheEvict(allEntries = true)
	public int deleteSysDict(SysDict sysDict) {
		return this.delete(sysDict);
	}

	@Cacheable(key="'allDictTable'")
	public Table<String,String, SysDict> findAllDictTable(){
		List<SysDict> dictList = this.select(new SysDict());
		Table<String,String, SysDict> tableDicts = HashBasedTable.create();
		for(SysDict dict : dictList){
			tableDicts.put(dict.getType(), dict.getValue(), dict);
		}
		return tableDicts;
	}
	
	@Cacheable(key="'allDictMultimap'")
	public Multimap<String, SysDict> findAllMultimap(){
		List<SysDict> dictList = this.select(new SysDict());
		Multimap<String, SysDict> multimap = ArrayListMultimap.create();
		for(SysDict dict : dictList){
			multimap.put(dict.getType(), dict);
		}
		return multimap;
	}

}
