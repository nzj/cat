package com.zsCat.web.sys.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.zsCat.common.utils.PageUtil;
import com.zsCat.web.sys.model.SysRole;
import com.zsCat.web.sys.model.SysUser;
import com.zsCat.web.sys.service.SysRoleService;
import com.zsCat.web.sys.service.SysUserService;

@Controller
@RequestMapping("sysuser")
public class SysUserController {

	@Resource
	private SysUserService sysUserService;
	@Resource
	private SysRoleService sysRoleService;
	
	/**
	 * 跳转到用户管理
	* @return
	 */
	@RequestMapping(value = "add")
	public String toSysUser(Model model){
		Map<Long, SysRole> rolesMap = sysRoleService.findCurUserRoleMap();
		model.addAttribute("rolesMap", rolesMap);
		return "sys/user/save";
	}
	
	
	/**
	 * 保存用户
	* @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> save(Model model, @ModelAttribute("sysUser") SysUser sysUser){
		Map<String, Object> msg = new HashMap<String, Object>();
//		 if(result.hasErrors()){
//			 msg.put("error", result.getFieldErrors().get(0));
//			 System.out.println(result.getAllErrors().get(0));
//		 }
		 try {
			sysUserService.saveSysUser(sysUser);
			msg.put("sucess", "数据保存成功");
		} catch (Exception e) {
			 msg.put("error", "数据保存失败");
			e.printStackTrace();
		}
		 return msg;
	}
	
	/**
	 * 删除用户
	* @param id 用户id
	* @return
	 */
	@RequestMapping(value="delete")
	public @ResponseBody String del(Long id){
		 sysUserService.deleteUser(id);
		return "redirect:/sysuser/list";
	}
	
	/**
	 * 用户列表
	* @param params
	* @param model
	* @return
	 */
	@RequestMapping(value = "list")
	public String list(HttpServletRequest req,@RequestParam(required=false , value="pageNum" ,defaultValue="1")int pageNum,
			@RequestParam(required=false , value="pageSize" ,defaultValue="15")int pageSize,
			@RequestParam Map<String, Object> params,Long[] roles,Model model){
		params.put("roles", StringUtils.join(roles,','));
		params.put("pageNum", pageNum);
		params.put("pageSize", pageSize);
		PageInfo<SysUser> page = sysUserService.findPageInfo(params);
		StringBuffer param=new StringBuffer(); // 查询参数
		model.addAttribute("page", page);
		model.addAttribute("pageCode",PageUtil.genMeiZiPagination(req.getContextPath()+"/sysuser/list", sysUserService.selectCount(new SysUser()), pageNum, pageSize, param.toString()));
		return "sys/user/list";
	}
	@RequestMapping(value="edit")
	public String showLayer(Long id, Model model){
		SysUser testUser = sysUserService.selectByPrimaryKey(id);
		model.addAttribute("user", testUser);
		Map<Long, SysRole> rolesMap = Maps.newHashMap(),findUserRoleMap = null;
		findUserRoleMap = sysRoleService.findUserRoleMapByUserId(id);
		rolesMap.putAll(sysRoleService.findCurUserRoleMap());
		rolesMap.putAll(findUserRoleMap);
		model.addAttribute("rolesMap", rolesMap)
			.addAttribute("findUserRoleMap", findUserRoleMap);
		return "sys/user/save";
	}
	/**
	 * 弹窗
	* @param id 用户id
	* @param mode 模式
	* @return
	 */
	@RequestMapping(value="{mode}/showlayer",method=RequestMethod.POST)
	public String showLayer(Long id,@PathVariable("mode") String mode, Model model){
		SysUser user = sysUserService.selectByPrimaryKey(id);;
		List<SysRole> roles = null;
		Map<Long, SysRole> rolesMap = Maps.newHashMap(),findUserRoleMap = null;
		if(StringUtils.equals("detail", mode)){
			roles = sysRoleService.findUserRoleListByUserId(id);
			model.addAttribute("roles", roles);
		}
		if(StringUtils.equals("edit", mode)){
			findUserRoleMap = sysRoleService.findUserRoleMapByUserId(id);
			rolesMap.putAll(sysRoleService.findCurUserRoleMap());
			rolesMap.putAll(findUserRoleMap);
			model.addAttribute("rolesMap", rolesMap)
				.addAttribute("findUserRoleMap", findUserRoleMap);
		}
		model.addAttribute("user", user);
		return mode.equals("detail")?"sys/user/user-detail":"sys/user/user-save";
	}
	
	/**
	 * 验证用户名是否存在
	* @param param
	* @return
	 */
	@RequestMapping(value="checkname",method=RequestMethod.POST)
	public @ResponseBody Map<String, Object> checkName(String param){
		Map<String, Object> msg = new HashMap<String, Object>();
		SysUser sysUser = new SysUser();
		sysUser.setUsername(param);
		int count = sysUserService.selectCount(sysUser);
		if(count>0){
			msg.put("info", "此登录名太抢手了,请换一个!");
			msg.put("status", "n");
		}else{
			msg.put("status", "y");
		}
		return msg;
	}
	
}
