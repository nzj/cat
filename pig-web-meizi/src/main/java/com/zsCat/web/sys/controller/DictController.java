package com.zsCat.web.sys.controller;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.zsCat.common.utils.PageUtil;
import com.zsCat.web.sys.model.SysDict;
import com.zsCat.web.sys.service.SysDictService;
import com.zsCat.web.test.model.TestUser;

@Controller
@RequestMapping("dict")
public class DictController {

	@Resource
	private SysDictService sysDictService;
	
	@RequestMapping(value="add")
	public String toDict(Model model){
		return "sys/dict/save";
	}
	
	@RequestMapping(value="edit")
	public String edit(Long id, Model model){
		SysDict dict = sysDictService.selectByPrimaryKey(id);
		model.addAttribute("dict", dict);
		return "sys/dict/save";
	}
	/**
	 * 添加或更新区域
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "save")
	public @ResponseBody  Map<String, Object> save(@ModelAttribute SysDict sysDict) {
		Map<String, Object> msg = new HashMap<String, Object>();
//		 if(result.hasErrors()){
//			 msg.put("error", result.getFieldErrors().get(0));
//			 System.out.println(result.getAllErrors().get(0));
//		 }
		 try {
			 sysDictService.saveSysdict(sysDict);
			msg.put("sucess", "数据保存成功");
		} catch (Exception e) {
			 msg.put("error", "数据保存失败");
			e.printStackTrace();
		}
		 return msg;
	}
	
	/**
	 * 删除字典
	* @param id
	* @return
	 */
	@RequestMapping(value="delete")
	public @ResponseBody String del(@ModelAttribute SysDict sysDict){
		 sysDictService.deleteSysDict(sysDict);
		return "redirect:/dict/list";
	}
	
	/**
	 * 分页显示字典table
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "list")
	public String list(HttpServletRequest req,@RequestParam(required=false , value="page" ,defaultValue="1")int pageNum,
			@RequestParam(required=false , value="pageSize" ,defaultValue="15")int pageSize,@ModelAttribute SysDict sysDict, Model model) {
		PageInfo<SysDict> page = sysDictService.selectPage(pageNum, pageSize, sysDict);
		StringBuffer param=new StringBuffer(); // 查询参数
		model.addAttribute("page", page);
		model.addAttribute("pageCode",PageUtil.genMeiZiPagination(req.getContextPath()+"/testUser/list", sysDictService.selectCount(sysDict), pageNum, pageSize, param.toString()));
		return "sys/dict/list";
	}
	
	@RequestMapping(value="{mode}/showlayer",method=RequestMethod.POST)
	public String showLayer(Long id, Model model){
		SysDict dict = sysDictService.selectByPrimaryKey(id);
		model.addAttribute("dict", dict);
		return "sys/dict/dict-save";
	}
	
}
