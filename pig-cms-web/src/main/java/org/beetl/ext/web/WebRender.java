/*** Eclipse Class Decompiler plugin, copyright (c) 2012 Chao Chen (cnfree2000@hotmail.com) ***//*
package org.beetl.ext.web;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.exception.BeetlException;

import com.zs.pig.common.constant.ZsCatConstant;

public class WebRender {
	GroupTemplate gt = null;

	public WebRender(GroupTemplate gt) {
		this.gt = gt;
	}

	public void render(String key, HttpServletRequest request,
			HttpServletResponse response, Object[] args) {
		Writer writer = null;
		OutputStream os = null;
		String ajaxId = null;
		Template template = null;
		try {
			int ajaxIdIndex = key.lastIndexOf("#");
			if (ajaxIdIndex != -1) {
				ajaxId = key.substring(ajaxIdIndex + 1);
				key = key.substring(0, ajaxIdIndex);
				template = this.gt.getAjaxTemplate(key, ajaxId);
			} else {
				template = this.gt.getTemplate(key);
			}

			Enumeration attrs = request.getAttributeNames();

			while (attrs.hasMoreElements()) {
				String attrName = (String) attrs.nextElement();
				template.binding(attrName, request.getAttribute(attrName));
			}

			WebVariable webVariable = new WebVariable();
			webVariable.setRequest(request);
			webVariable.setResponse(response);
			template.binding("session",
					new SessionWrapper(webVariable.getRequest()));

			template.binding("servlet", webVariable);
			template.binding("request", request);
			template.binding("ctxPath", request.getContextPath());
			template.binding("imgServer", ZsCatConstant.IMGSERVER);;
			modifyTemplate(template, key, request, response, args);

			if (this.gt.getConf().isDirectByteOutput()) {
				os = response.getOutputStream();
				template.renderTo(os);
			} else {
				writer = response.getWriter();
				template.renderTo(writer);
			}

		} catch (IOException e) {
			handleClientError(e);
		} catch (BeetlException e) {
			handleBeetlException(e);
		} finally {
			try {
				if (writer != null)
					writer.flush();
				if (os != null) {
					os.flush();
				}
			} catch (IOException e) {
				handleClientError(e);
			}
		}
	}

	protected void modifyTemplate(Template template, String key,
			HttpServletRequest request, HttpServletResponse response,
			Object[] args) {
	}

	protected void handleClientError(IOException ex) {
	}

	protected void handleBeetlException(BeetlException ex) {
		throw ex;
	}
}*/