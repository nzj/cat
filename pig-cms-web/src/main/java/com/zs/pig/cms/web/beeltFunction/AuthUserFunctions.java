package com.zs.pig.cms.web.beeltFunction;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.zs.pig.cms.api.model.CmsArticle;
import com.zs.pig.cms.api.model.CmsCategory;
import com.zs.pig.cms.api.service.CmsArticleService;
import com.zs.pig.cms.api.service.CmsCategoryService;

@Component
public class AuthUserFunctions {

	@Resource
	private CmsArticleService CmsArticleService;
	@Resource
	private CmsCategoryService  CmsCategoryService;
	
	public List<CmsArticle> getArticleByCate(Long id){
		CmsArticle record=new CmsArticle();
		record.setCategoryId(id);
		return CmsArticleService.select(record, "createdate desc");
	}
	public List<CmsCategory> getCateGory(String model){
		CmsCategory CmsCategory=new CmsCategory();
		CmsCategory.setIsshow(1);
		CmsCategory.setModule(model);
		return CmsCategoryService.select(CmsCategory);
	}
	
	
}
