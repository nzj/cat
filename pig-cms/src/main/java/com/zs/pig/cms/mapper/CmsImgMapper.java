//Powered By if, Since 2014 - 2020

package com.zs.pig.cms.mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.cms.api.model.CmsImg;


/**
 * 
 * @author zsCat 2016-10-18 19:49:20
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	我的官网
 */
public interface CmsImgMapper extends Mapper<CmsImg>{

}
