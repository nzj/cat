package com.zs.pig.cms.web1.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.zs.pig.base.api.model.SysDict;
import com.zs.pig.base.api.service.SysDictService;
import com.zs.pig.blog.api.model.Blog;
import com.zs.pig.blog.api.service.BlogService;
import com.zs.pig.cms.api.model.CmsArticle;
import com.zs.pig.cms.api.model.CmsCategory;
import com.zs.pig.cms.api.model.Nav;
import com.zs.pig.cms.api.service.CmsArticleService;
import com.zs.pig.cms.api.service.CmsCategoryService;
import com.zs.pig.cms.api.service.CmsImgService;
import com.zs.pig.cms.api.service.NavService;
	/**
	 * 
	 * @author zs 2016-5-5 11:33:51
	 * @Email: 951449465@qq.com
	 * @version 4.0v
	 *	我的blog
	 */
@Controller
@RequestMapping("cms")
public class IndexCmsController {

	
	@Resource
	private CmsArticleService CmsArticleService;
	@Resource
	private CmsCategoryService CmsCategoryService;
	@Resource
	private NavService NavService;
	@Resource
	private CmsImgService CmsImgService;
	@Resource
	private BlogService BlogService;
	@Resource
	private SysDictService SysDictService;
	/**
	 * 请求主页
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/index")
	public ModelAndView index()throws Exception{
		System.out.println( SysDictService.findAllMultimap().get("cms_wap_type"));
		ModelAndView mav=new ModelAndView();
		mav.setViewName("cms/index");
		return mav;
	}
	/**
	 * 文章列表
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/articleDetail/{id}")
	public String house(@PathVariable("id") Long id, Model model)throws Exception{
		CmsArticle article=CmsArticleService.selectByPrimaryKey(id);
		model.addAttribute("article",article);
		return "cms/forumDetail";
	}
	
	@RequestMapping("/index/ajax/bna")
	public String ajaxindex(HttpServletRequest request) {
		try {
			String id = request.getParameter("order");
			if (id != null && !id.equals("")) {
				CmsArticle a=new CmsArticle();
				PageInfo<CmsArticle> artList =null;
				if("reco".equals(id)){
					a.setWeight(1);
					artList = CmsArticleService.selectPage(1, 40, a,"");
				}else if("late".equals(id)){
					artList = CmsArticleService.selectPage(1, 40, a,"createdate desc");
				}else if("hot".equals(id)){
					artList = CmsArticleService.selectPage(1, 40, a,"hits desc");
				}
				
				request.setAttribute("artList", artList.getList());
			}
		} catch (Exception e) {

		}
		return "pc/ajax-index";
	}
	
	
	@RequestMapping(value = "blog")
	public String blog(@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="12")Integer pageSize,@ModelAttribute Blog CmsArticle, Model model) {
		
		
		PageInfo<Blog> artList=BlogService.selectPage(pageNum, pageSize, CmsArticle);
		model.addAttribute("page", artList);
		
		return "pc/blog";
	}
	/**
	 * 产品详细信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/blogDetail/{id}")
	public ModelAndView blogDetails(@PathVariable("id") Long id,HttpServletRequest request)throws Exception{
		ModelAndView mav=new ModelAndView();
				Blog blog=BlogService.selectByPrimaryKey(id);
				mav.addObject("blog", blog);
		mav.setViewName("pc/blogDetail");
		return mav;
	}
}
