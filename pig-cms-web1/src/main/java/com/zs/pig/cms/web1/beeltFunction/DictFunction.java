package com.zs.pig.cms.web1.beeltFunction;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import com.zs.pig.base.api.model.SysDict;
import com.zs.pig.base.api.service.SysDictService;

/**
 * 字典方法
 * 传入参数 type
 */
@Component
public class DictFunction{
	
	@Resource
	private SysDictService SysDictService;
	
	/**
	 * 根据类型和键值得到字典实体
	* @param type 如sys_data_scope等
	* @param value 
	 */
	public SysDict getDictByTypeAndValue(String type,String value){
		Table<String,String, SysDict> tableDicts = SysDictService.findAllDictTable();
		return tableDicts.get(type, value);
	}
	
	/**
	 * 根据类型得到字典列表
	* @param type 如sys_data_scope等
	 */
	public List<SysDict> getDictListByType(String type){
		List<SysDict> dictList = SysDictService.select(new SysDict());
		Multimap<String, SysDict> multimap = ArrayListMultimap.create();
		for(SysDict dict : dictList){
			multimap.put(dict.getType(), dict);
		}
		return  (List<SysDict>)multimap.get(type);
	//	return (List<SysDict>) SysDictService.findAllMultimap().get(type);
	}
	
	/**
	 * 全部字典
	 */
	public Collection<String> getAllDictType(){
		return SysDictService.findAllMultimap().keySet();
	}

}
