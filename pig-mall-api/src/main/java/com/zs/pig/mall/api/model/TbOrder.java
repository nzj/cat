package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;
/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_order")
public class TbOrder extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String orderId;
    private String payment;

    private Integer paymentType;

    private String postFee;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private Date paymentTime;

    private Date consignTime;

    private Date endTime;

    private Date closeTime;

    private String shippingName;

    private String shippingCode;

    private Long userId;

    private String buyerMessage;

    private String buyerNick;

    private Integer buyerRate;

    public String getOrderId() {
        return this.getString("orderId");
    }

    public void setOrderId(String orderId) {
        this.set("orderId", orderId);
    }

    public String getPayment() {
        return this.getString("payment");
    }

    public void setPayment(String payment) {
        this.set("payment", payment);
    }

    public Integer getPaymentType() {
        return this.getInteger("paymentType");
    }

    public void setPaymentType(Integer paymentType) {
        this.set("paymentType", paymentType);
    }

    public String getPostFee() {
        return this.getString("postFee");
    }

    public void setPostFee(String postFee) {
        this.set("postFee", postFee);
    }

    public Integer getStatus() {
        return this.getInteger("status");
    }

    public void setStatus(Integer status) {
        this.set("status", status);
    }

    public Date getCreateTime() {
        return this.getDate("createTime");
    }

    public void setCreateTime(Date createTime) {
        this.set("createTime", createTime);
    }

    public Date getUpdateTime() {
        return this.getDate("updateTime");
    }

    public void setUpdateTime(Date updateTime) {
        this.set("updateTime", updateTime);
    }

    public Date getPaymentTime() {
        return this.getDate("paymentTime");
    }

    public void setPaymentTime(Date paymentTime) {
        this.set("paymentTime", paymentTime);
    }

    public Date getConsignTime() {
        return this.getDate("consignTime");
    }

    public void setConsignTime(Date consignTime) {
        this.set("consignTime", consignTime);
    }

    public Date getEndTime() {
        return this.getDate("endTime");
    }

    public void setEndTime(Date endTime) {
        this.set("endTime", endTime);
    }

    public Date getCloseTime() {
        return this.getDate("closeTime");
    }

    public void setCloseTime(Date closeTime) {
        this.set("closeTime", closeTime);
    }

    public String getShippingName() {
        return this.getString("shippingName");
    }

    public void setShippingName(String shippingName) {
        this.set("shippingName", shippingName);
    }

    public String getShippingCode() {
        return this.getString("shippingCode");
    }

    public void setShippingCode(String shippingCode) {
        this.set("shippingCode", shippingCode);
    }

    public Long getUserId() {
        return this.getLong("userId");
    }

    public void setUserId(Long userId) {
        this.set("userId", userId);
    }

    public String getBuyerMessage() {
        return this.getString("buyerMessage");
    }

    public void setBuyerMessage(String buyerMessage) {
        this.set("buyerMessage", buyerMessage);
    }

    public String getBuyerNick() {
        return this.getString("buyerNick");
    }

    public void setBuyerNick(String buyerNick) {
        this.set("buyerNick", buyerNick);
    }

    public Integer getBuyerRate() {
        return this.getInteger("buyerRate");
    }

    public void setBuyerRate(Integer buyerRate) {
        this.set("buyerRate", buyerRate);
    }
}