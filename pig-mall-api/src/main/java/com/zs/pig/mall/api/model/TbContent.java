package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;
/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_content")
public class TbContent extends BaseEntity {

	private static final long serialVersionUID = 1L;

    private Long categoryId;

    private String title;

    private String subTitle;

    private String titleDesc;

    private String url;

    private String pic;

    private String pic2;

    private Date created;

    private Date updated;

    private String content;

   
    public Long getCategoryId() {
        return this.getLong("categoryId");
    }

    public void setCategoryId(Long categoryId) {
        this.set("categoryId", categoryId);
    }

    public String getTitle() {
        return this.getString("title");
    }

    public void setTitle(String title) {
        this.set("title", title);
    }

    public String getSubTitle() {
        return this.getString("subTitle");
    }

    public void setSubTitle(String subTitle) {
    	this.set("subTitle", subTitle);
    }

    public String getTitleDesc() {
        return this.getString("titleDesc");
    }

    public void setTitleDesc(String titleDesc) {
       this.set("titleDesc", titleDesc);
    }

    public String getUrl() {
        return this.getString("url");
    }

    public void setUrl(String url) {
       this.set("url", url);
    }

    public String getPic() {
        return this.getString("pic");
    }

    public void setPic(String pic) {
        this.set("pic", pic);
    }

    public String getPic2() {
        return this.getString("pic2");
    }

    public void setPic2(String pic2) {
       this.set("pic2", pic2);
    }

    public Date getCreated() {
        return this.getDate("created");
    }

    public void setCreated(Date created) {
        this.set("created", created);
    }

    public Date getUpdated() {
        return this.getDate("updated");
    }

    public void setUpdated(Date updated) {
        this.set("updated",updated);
    }

    public String getContent() {
        return this.getString("content");
    }

    public void setContent(String content) {
        this.set("content", content);
    }
}