package com.zs.pig.mall.api.model;

import java.util.Date;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;
/**
 * 2016-04-22
 * @author zsCat
 */

@SuppressWarnings({ "unused"})
@Table(name="tb_item_param")
public class TbItemParam extends BaseEntity {

	private static final long serialVersionUID = 1L;

    private Long itemCatId;

    private Date created;

    private Date updated;

    private String paramData;

  

    public Long getItemCatId() {
        return this.getLong("itemCatId");
    }

    public void setItemCatId(Long itemCatId) {
        this.set("itemCatId", itemCatId);
    }

    public Date getCreated() {
        return this.getDate("created");
    }

    public void setCreated(Date created) {
        this.set("created", created);
    }

    public Date getUpdated() {
        return this.getDate("updated");
    }

    public void setUpdated(Date updated) {
        this.set("updated",updated);
    }

    public String getParamData() {
        return this.getString("paramData");
    }

    public void setParamData(String paramData) {
        this.set("paramData", paramData);
    }
}