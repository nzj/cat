package com.zs.pig.mall.api.service;

import com.zs.pig.common.base.BaseService;
import com.zs.pig.mall.api.model.TbItemParamItem;

public interface ItemParamItemService extends BaseService<TbItemParamItem>{

	String getItemParamByItemId(Long itemId);
}
