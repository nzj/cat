package com.zs.pig.blog.portl.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.zsCat.common.lucene.BlogIndex;
import com.zsCat.common.utils.StringUtil;
import com.zsCat.web.blog.model.Blog;
import com.zsCat.web.blog.model.BlogType;
import com.zsCat.web.blog.model.Comment;
import com.zsCat.web.blog.service.BlogService;
import com.zsCat.web.blog.service.BlogTemplateService;
import com.zsCat.web.blog.service.BlogTypeService;
import com.zsCat.web.blog.service.BloggerService;
import com.zsCat.web.blog.service.CommentService;
import com.zsCat.web.blog.service.LinkService;
import com.zsCat.web.cms.model.CmsArticle;
import com.zsCat.web.cms.model.CmsCategory;
import com.zsCat.web.cms.model.CmsImg;
import com.zsCat.web.cms.service.CmsArticleService;
import com.zsCat.web.cms.service.CmsCategoryService;
import com.zsCat.web.cms.service.CmsImgService;
import com.zsCat.web.gw.model.GwInfo;
import com.zsCat.web.gw.model.Nav;
import com.zsCat.web.gw.model.Product;
import com.zsCat.web.gw.service.GwInfoService;
import com.zsCat.web.gw.service.NavService;
import com.zsCat.web.gw.service.ProductService;
import com.zsCat.web.gw.service.ProductTypeService;
	/**
	 * 
	 * @author zs 2016-5-5 11:33:51
	 * @Email: 951449465@qq.com
	 * @version 4.0v
	 *	我的blog
	 */
@Controller
@RequestMapping("cms")
public class IndexCmsController {

	@Resource
	private BlogTemplateService BlogTemplateService;
	
	// 博客索引
	private BlogIndex blogIndex=new BlogIndex();
		
	@Resource
	private BlogService blogService;
	@Resource
	private CommentService CommentService;
	@Resource
	private BlogTypeService BlogTypeService;
	@Resource
	private BloggerService BloggerService;
	@Resource
	private LinkService LinkService;
	@Resource
	private CmsArticleService CmsArticleService;
	@Resource
	private CmsCategoryService CmsCategoryService;
	@Resource
	private NavService NavService;
	@Resource
	private CmsImgService CmsImgService;
	/**
	 * 请求主页
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/index")
	public ModelAndView index(@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="12")Integer pageSize)throws Exception{
		ModelAndView mav=new ModelAndView();
		//首页顶部导航
		Nav nav=new Nav();
		nav.setDisplay(true);
		nav.setType("1");
		mav.addObject("navList", NavService.select(nav, "sequence asc"));
		PageInfo<CmsImg> imgList=CmsImgService.selectPage(1, 12, null);
		mav.addObject("imgList", imgList);
		
		List<Blog> blogList = blogService.select(null);
		for(Blog blog:blogList){
			BlogType blogType=BlogTypeService.selectByPrimaryKey(blog.getTypeid());
			blog.setBlogType(blogType);
			String blogInfo=blog.getContent();
			Document doc=Jsoup.parse(blogInfo);
			Elements jpgs=doc.select("img[src]"); //　查找扩展名是jpg的图片
			if(jpgs!=null && jpgs.size()>0){
			Element jpg=jpgs.get(0);
				if(jpgs!=null && jpg!=null){
					String linkHref = jpg.attr("src");
					blog.setImg(linkHref);
				}else{
					blog.setImg("static/bbs/img/menu.png");
				}
			}
		}
		mav.addObject("artList", blogList);
		
		PageInfo<BlogType> cateList=BlogTypeService.selectPage(1, 10, null);
		mav.addObject("cateList", cateList);		
		mav.setViewName("cms/index");
		return mav;
	}
	
	@RequestMapping(value = "newsList")
	public String newList(
			@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="12")Integer pageSize,
			@ModelAttribute CmsArticle CmsArticle,
			HttpServletRequest request, Model model) {
		//首页顶部导航
		Nav nav=new Nav();
		nav.setDisplay(true);
		nav.setType("1");
		model.addAttribute("navList", NavService.select(nav, "sequence asc"));
		String cateId=request.getParameter("cateId");
		if(cateId!=null && cateId!=""){
			CmsArticle.setCategoryId(Long.parseLong(cateId));
		}
		PageInfo<CmsArticle> artList=CmsArticleService.selectPage(pageNum, pageSize, CmsArticle);
		for(CmsArticle blog:artList.getList()){
			CmsCategory cmsCategory=CmsCategoryService.selectByPrimaryKey(blog.getCategoryId());
			blog.setCmsCategory(cmsCategory);
			String blogInfo=blog.getDescription();
			Document doc=Jsoup.parse(blogInfo);
			Elements jpgs=doc.select("img[src]"); //　查找扩展名是jpg的图片
			if(jpgs!=null && jpgs.size()>0){
			Element jpg=jpgs.get(0);
				if(jpgs!=null && jpg!=null){
					String linkHref = jpg.attr("src");
					blog.setImage(linkHref);
				}
			}else{
				blog.setImage("static/bbs/img/shop.png");
			}
			
		}
		model.addAttribute("artList", artList);
		return "cms/news";
	}
	/**
	 * 产品详细信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/news/{id}")
	public ModelAndView newsDetails(@PathVariable("id") Long id,HttpServletRequest request)throws Exception{
		ModelAndView mav=new ModelAndView();
				//首页顶部导航
				Nav nav=new Nav();
				nav.setDisplay(true);
				nav.setType("1");
				mav.addObject("navList", NavService.select(nav, "sequence asc"));
				
				CmsArticle article=CmsArticleService.selectByPrimaryKey(id);
				mav.addObject("article", article);
		mav.setViewName("cms/newsDetail");
		return mav;
	}
	/**
	 * 论坛菜单
	 * @param pageNum
	 * @param pageSize
	 * @param CmsArticle
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "forumAll")
	public String forumAll(@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="12")Integer pageSize,@ModelAttribute CmsArticle CmsArticle, Model model) {
		PageInfo<CmsArticle> artList=CmsArticleService.selectPage(pageNum, pageSize, CmsArticle);
		model.addAttribute("page", artList);
		
		//首页顶部导航
				Nav nav=new Nav();
				nav.setDisplay(true);
				nav.setType("1");
				model.addAttribute("navList", NavService.select(nav, "sequence asc"));
		return "cms/forumAll";
	}
	/**
	 * 论坛列表
	 * @param pageNum
	 * @param pageSize
	 * @param CmsArticle
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "forumList")
	public String forumList(@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="12")Integer pageSize,@ModelAttribute Blog blog,
			Model model,HttpServletRequest req) {
		String id=req.getParameter("id");
		if(!"".equals(id) && id!=null){
			blog.setTypeid(Long.parseLong(id));
		}
		PageInfo<Blog> artList=blogService.selectPage(0, 10000, blog);
		model.addAttribute("page", artList);
		PageInfo<CmsCategory> cateList=CmsCategoryService.selectPage(1, 10, null);
		model.addAttribute("cateList", cateList);
		
		//首页顶部导航
				Nav nav=new Nav();
				nav.setDisplay(true);
				nav.setType("1");
				model.addAttribute("navList", NavService.select(nav, "sequence asc"));
		return "cms/forumList";
	}
	/**
	 * 论坛详细信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/forumDetail/{id}")
	public ModelAndView forumDetail(@PathVariable("id") Long id,HttpServletRequest request)throws Exception{
		ModelAndView mav=new ModelAndView();
		//首页顶部导航
				Nav nav=new Nav();
				nav.setDisplay(true);
				nav.setType("1");
				mav.addObject("navList", NavService.select(nav, "sequence asc"));
		mav.setViewName("cms/forumDetail");
		
		PageInfo<Blog> artList=blogService.selectPage(1, 102, null);
		mav.addObject("page", artList);
		
		Blog article=blogService.selectByPrimaryKey(id);
		mav.addObject("article", article);
		return mav;
	}
	
	/**
	 * 导读菜单
	 * @param pageNum
	 * @param pageSize
	 * @param CmsArticle
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "daodu")
	public String daodu(@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="12")Integer pageSize,@ModelAttribute CmsArticle CmsArticle, Model model) {
		PageInfo<CmsArticle> artList=CmsArticleService.selectPage(pageNum, pageSize, CmsArticle);
		model.addAttribute("page", artList);
		//首页顶部导航
				Nav nav=new Nav();
				nav.setDisplay(true);
				nav.setType("1");
				model.addAttribute("navList", NavService.select(nav, "sequence asc"));
		return "cms/daodu";
	}
	/**
	 * 图片瀑布流 菜单
	 * @param pageNum
	 * @param pageSize
	 * @param CmsArticle
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "imgList")
	public String imgList(@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="12")Integer pageSize,@ModelAttribute CmsImg CmsImg, Model model) {
		PageInfo<CmsImg> artList=CmsImgService.selectPage(pageNum, pageSize, CmsImg);
		model.addAttribute("page", artList);
		//首页顶部导航
				Nav nav=new Nav();
				nav.setDisplay(true);
				nav.setType("1");
				model.addAttribute("navList", NavService.select(nav, "sequence asc"));
				PageInfo<CmsImg> imgList=CmsImgService.selectPage(pageNum, pageSize, null);
				model.addAttribute("page", imgList);
		return "cms/img";
	}
}
