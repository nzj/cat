package com.zs.pig.blog.portl.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.zs.pig.blog.api.model.Blog;
import com.zs.pig.blog.api.model.BlogType;
import com.zs.pig.blog.api.model.Comment;
import com.zs.pig.blog.api.service.BlogService;
import com.zs.pig.blog.api.service.BlogTypeService;
import com.zs.pig.blog.api.service.BloggerService;
import com.zs.pig.blog.api.service.CommentService;
import com.zs.pig.blog.portl.util.SysUserUtils;
import com.zs.pig.common.utils.IPUtils;

@Controller
@RequestMapping("person")
public class PersonController {

	@Resource
	private BlogService blogService;
	@Resource
	private CommentService CommentService;
	@Resource
	private BlogTypeService BlogTypeService;
	@Resource
	private BloggerService BloggerService;
	
	@RequestMapping("/index")
	public ModelAndView index()throws Exception{
		ModelAndView mav=new ModelAndView();
		mav.addObject("test", "shenzhuan");
		mav.setViewName("blog/community/user/index");
		return mav;
	
	}
	/**
	 * 发布问题
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/adds")
	public ModelAndView adds()throws Exception{
		ModelAndView mav=new ModelAndView();
		List<BlogType> typeList=BlogTypeService.select(new BlogType());
		mav.addObject("typeList", typeList);
		mav.setViewName("blog/community/jie/add");
		return mav;
	}
	@RequestMapping("/saves")
	public String saves(String vercode,String title,String content,Long typeid,HttpServletRequest req)throws Exception{
		if (!StringUtils.equalsIgnoreCase("3711111", vercode)) {
			return "redirect:/front/blog/adds";
		}
		Blog blog=new Blog();
		blog.setTitle(title);
		blog.setState(0);
		blog.setContent(content);
		blog.setTypeid(typeid);
		if(SysUserUtils.getSessionBloggerUser()!=null){
			blog.setBloggerId(SysUserUtils.getSessionBloggerUser().getId());
		}else{
			blog.setBloggerId(1L);
		}
		
		blog.setReleasedate(new Date());
		blog.setSummary(title);
		blogService.insertSelective(blog);
		return "redirect:/front/blog/indexs";
		
	}
	@RequestMapping("/activate")
	public ModelAndView activate()throws Exception{
		ModelAndView mav=new ModelAndView();
		mav.addObject("test", "shenzhuan");
		mav.setViewName("blog/community/user/activate");
		return mav;
	
	}
	@RequestMapping("/question")
	public ModelAndView question()throws Exception{
		ModelAndView mav=new ModelAndView();
		mav.addObject("test", "shenzhuan");
		mav.setViewName("blog/community/user/question");
		return mav;
	
	}
	@RequestMapping("/message")
	public ModelAndView message()throws Exception{
		ModelAndView mav=new ModelAndView();
		mav.addObject("test", "shenzhuan");
		mav.setViewName("blog/community/user/message");
		return mav;
	
	}
	@RequestMapping("/home")
	public ModelAndView home()throws Exception{
		ModelAndView mav=new ModelAndView();
		Blog blog=new Blog();
		blog.setBloggerId(SysUserUtils.getSessionBloggerUser().getId());
		PageInfo<Blog> page=blogService.selectPage(1, 15, blog);
		mav.addObject("page", page);
		
		Comment comment=new Comment();
		comment.setBloggerId(SysUserUtils.getSessionBloggerUser().getId());
		PageInfo<Comment> commList=CommentService.selectPage(1, 15, comment);
		mav.addObject("commList", commList);
		mav.setViewName("blog/community/user/home");
		return mav;
	
	}
	@RequestMapping("/set")
	public ModelAndView set()throws Exception{
		ModelAndView mav=new ModelAndView();
		mav.addObject("test", "shenzhuan");
		mav.setViewName("blog/community/user/set");
		return mav;
	
	}
	@RequestMapping("/reply")
	public String reply(String content,String blogtitle,
			Long blogid,HttpServletRequest req)throws Exception{
		Comment com=new Comment();
		com.setCommentdate(new Date());
		com.setBloggerId(SysUserUtils.getSessionBloggerUser().getId());
		com.setContent(content);
		com.setBlogid(blogid);
		com.setTitle(blogtitle);
		com.setUserip(IPUtils.getClientAddress(req));
		com.setBlogger(SysUserUtils.getSessionBloggerUser().getNickname());
		CommentService.insertSelective(com);
		return "redirect:/front/blog/shequDetail/"+blogid;
	
	}
	
	
}
