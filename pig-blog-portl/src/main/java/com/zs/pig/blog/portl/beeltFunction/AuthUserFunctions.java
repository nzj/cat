package com.zs.pig.blog.portl.beeltFunction;

import java.util.List;

import javax.annotation.Resource;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import com.zs.pig.blog.api.model.Blog;
import com.zs.pig.blog.api.model.BlogType;
import com.zs.pig.blog.api.model.Blogger;
import com.zs.pig.blog.api.service.BlogService;
import com.zs.pig.blog.portl.util.SysUserUtils;

@Component
public class AuthUserFunctions {

	
	@Resource
	private BlogService blogService;
	/**
	 * 登录用户
	* @return
	 */
	public Blogger getLoginBlogger(){
		return (Blogger) SysUserUtils.getSessionBloggerUser();
	}
	
	/**
	 * 是否为超级管理员
	* @return
	 */
	public boolean isBloggerLogin(){
		return SysUserUtils.getSessionBloggerUser()!=null;
	}
	public List<Blog> getArticleListByTypeId(Long id){
		Blog b=new Blog();
		b.setTypeid(id);
		List<Blog> blogList= blogService.select(b);
		for(Blog blog:blogList){
			String blogInfo=blog.getContent();
			Document doc=Jsoup.parse(blogInfo);
			Elements jpgs=doc.select("img[src]"); //　查找扩展名是jpg的图片
			if(jpgs!=null && jpgs.size()>0){
			Element jpg=jpgs.get(0);
				if(jpgs!=null && jpg!=null){
					String linkHref = jpg.attr("src");
					blog.setImg(linkHref);
				}else{
					blog.setImg("static/bbs/img/menu.png");
				}
			}
		}
		return blogList;	
	}
}
