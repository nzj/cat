﻿/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.6.26 : Database - shop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shop` /*!40100 DEFAULT CHARACTER SET utf8 */;


/*Table structure for table `gen_scheme` */

DROP TABLE IF EXISTS `gen_scheme`;

CREATE TABLE `gen_scheme` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `category` varchar(2000) DEFAULT NULL COMMENT '分类',
  `package_name` varchar(500) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `sub_module_name` varchar(30) DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(200) DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(100) DEFAULT NULL COMMENT '生成功能名（简写）',
  `function_author` varchar(100) DEFAULT NULL COMMENT '生成功能作者',
  `gen_table_id` bigint(11) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL COMMENT '创建者',
  `create_date` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '更新者',
  `update_date` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成器生成方案';

/*Data for the table `gen_scheme` */

/*Table structure for table `gen_table` */

DROP TABLE IF EXISTS `gen_table`;

CREATE TABLE `gen_table` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `comments` varchar(500) DEFAULT NULL COMMENT '描述',
  `class_name` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `parent_table` varchar(200) DEFAULT NULL COMMENT '关联父表',
  `parent_table_fk` varchar(100) DEFAULT NULL COMMENT '关联父表外键',
  `create_by` int(11) DEFAULT NULL COMMENT '创建者',
  `create_date` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '更新者',
  `update_date` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成器业务表';

/*Data for the table `gen_table` */

/*Table structure for table `gen_table_column` */

DROP TABLE IF EXISTS `gen_table_column`;

CREATE TABLE `gen_table_column` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `gen_table_id` bigint(11) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `comments` varchar(500) DEFAULT NULL COMMENT '描述',
  `jdbc_type` varchar(100) DEFAULT NULL COMMENT 'JDBC类型',
  `myBatis_jdbc_type` varchar(50) DEFAULT NULL COMMENT 'mybatisJDBC类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` int(1) DEFAULT NULL COMMENT '是否主键(1：是、0：否)',
  `is_null` int(1) DEFAULT NULL COMMENT '是否可为空(1：是、0：否)',
  `is_insert` int(1) DEFAULT NULL COMMENT '是否为插入字段(1：是、0：否)',
  `is_edit` int(1) DEFAULT NULL COMMENT '是否编辑字段(1：是、0：否)',
  `is_list` int(1) DEFAULT NULL COMMENT '是否列表字段(1：是、0：否)',
  `is_query` int(1) DEFAULT NULL COMMENT '是否查询字段(1：是、0：否)',
  `query_type` varchar(200) DEFAULT NULL COMMENT '查询方式（等于、不等于、大于、小于、范围、左LIKE、右LIKE、左右LIKE）',
  `show_type` varchar(200) DEFAULT NULL COMMENT '字段生成方案（文本框、文本域、下拉框、复选框、单选框、字典选择、人员选择、部门选择、区域选择）',
  `dict_type` varchar(200) DEFAULT NULL COMMENT '字典类型',
  `settings` varchar(2000) DEFAULT NULL COMMENT '其它设置（扩展字段JSON）',
  `sort` int(11) DEFAULT NULL COMMENT '排序（升序）',
  `create_by` int(11) DEFAULT NULL COMMENT '创建者',
  `create_date` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '更新者',
  `update_date` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='表字段';

/*Data for the table `gen_table_column` */

/*Table structure for table `shop_activity_detail` */

DROP TABLE IF EXISTS `shop_activity_detail`;

CREATE TABLE `shop_activity_detail` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `activity_id` bigint(11) DEFAULT NULL,
  `item_id` bigint(11) DEFAULT NULL,
  `item_name` varchar(255) NOT NULL COMMENT '商品或团购名称',
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(255) NOT NULL COMMENT '店铺名称',
  `activity_detail_state` enum('0','1','2','3') NOT NULL DEFAULT '0' COMMENT '审核状态 0:(默认)待审核 1:通过 2:未通过 3:再次申请',
  `activity_detail_sort` tinyint(1) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `activity_id` (`activity_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='活动细节表';

/*Data for the table `shop_activity_detail` */

/*Table structure for table `shop_address` */

DROP TABLE IF EXISTS `shop_address`;

CREATE TABLE `shop_address` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `member_id` bigint(11) DEFAULT NULL,
  `true_name` varchar(50) NOT NULL COMMENT '会员姓名',
  `area_id` bigint(11) DEFAULT NULL,
  `city_id` bigint(11) DEFAULT NULL,
  `area_info` varchar(255) NOT NULL DEFAULT '' COMMENT '地区内容',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `tel_phone` varchar(20) DEFAULT NULL COMMENT '座机电话',
  `mob_phone` varchar(15) DEFAULT NULL COMMENT '手机电话',
  `is_default` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1默认收货地址',
  `province_id` bigint(11) DEFAULT NULL,
  `zip_code` int(50) DEFAULT NULL COMMENT '邮编',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='买家地址信息表';

/*Data for the table `shop_address` */

/*Table structure for table `shop_admin` */

DROP TABLE IF EXISTS `shop_admin`;

CREATE TABLE `shop_admin` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `admin_name` varchar(20) NOT NULL COMMENT '管理员名称',
  `admin_password` varchar(300) DEFAULT NULL,
  `admin_login_time` bigint(13) DEFAULT NULL COMMENT '登录时间',
  `admin_login_num` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数',
  `admin_is_super` int(1) NOT NULL DEFAULT '0' COMMENT '是否超级管理员',
  `admin_gid` int(4) DEFAULT '0' COMMENT '权限组ID',
  `is_del` int(4) DEFAULT '0' COMMENT '删除状态',
  `role_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表';

/*Data for the table `shop_admin` */

/*Table structure for table `shop_admin_log` */

DROP TABLE IF EXISTS `shop_admin_log`;

CREATE TABLE `shop_admin_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `content` varchar(50) NOT NULL COMMENT '操作内容',
  `admin_name` char(20) NOT NULL COMMENT '管理员',
  `admin_id` bigint(11) DEFAULT NULL,
  `ip` char(15) NOT NULL COMMENT 'IP',
  `url` varchar(50) NOT NULL DEFAULT '' COMMENT 'act&op',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员操作日志';

/*Data for the table `shop_admin_log` */

/*Table structure for table `shop_adv` */

DROP TABLE IF EXISTS `shop_adv`;

CREATE TABLE `shop_adv` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `ap_id` bigint(11) DEFAULT NULL,
  `adv_title` varchar(100) NOT NULL COMMENT '标题',
  `adv_url` varchar(255) DEFAULT NULL COMMENT '广告内容',
  `start_date` bigint(13) DEFAULT NULL COMMENT '广告开始时间',
  `end_date` bigint(13) DEFAULT NULL COMMENT '广告结束时间',
  `sort` int(10) unsigned DEFAULT NULL COMMENT '幻灯片排序',
  `click_num` int(11) unsigned DEFAULT NULL COMMENT '广告点击率',
  `is_allow` int(1) unsigned DEFAULT NULL COMMENT '会员购买的广告是否通过审核0未审核1审核已通过2审核未通过',
  `res_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告表';

/*Data for the table `shop_adv` */

/*Table structure for table `shop_adv_click` */

DROP TABLE IF EXISTS `shop_adv_click`;

CREATE TABLE `shop_adv_click` (
  `id` bigint(11) DEFAULT NULL,
  `ap_id` bigint(11) DEFAULT NULL,
  `click_year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '点击年份',
  `click_month` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '点击月份',
  `click_num` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '点击率',
  `adv_name` varchar(100) NOT NULL COMMENT '广告名称',
  `ap_name` varchar(100) NOT NULL COMMENT '广告位名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告点击率表';

/*Data for the table `shop_adv_click` */

/*Table structure for table `shop_adv_position` */

DROP TABLE IF EXISTS `shop_adv_position`;

CREATE TABLE `shop_adv_position` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `ap_name` varchar(100) NOT NULL COMMENT '广告位置名',
  `ap_intro` varchar(255) NOT NULL COMMENT '广告位简介',
  `ap_class` int(1) unsigned NOT NULL COMMENT '广告类别：0图片1文字2幻灯3Flash',
  `ap_display` int(1) unsigned NOT NULL COMMENT '广告展示方式：0幻灯片1多广告展示2单广告展示',
  `is_use` int(1) unsigned NOT NULL COMMENT '广告位是否启用：0不启用1启用',
  `ap_width` int(10) NOT NULL COMMENT '广告位宽度',
  `ap_height` int(10) NOT NULL COMMENT '广告位高度',
  `ap_key` varchar(50) DEFAULT NULL COMMENT '取值键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告位表';

/*Data for the table `shop_adv_position` */

/*Table structure for table `shop_area` */

DROP TABLE IF EXISTS `shop_area`;

CREATE TABLE `shop_area` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `area_name` varchar(50) NOT NULL COMMENT '地区名称',
  `area_parent_id` bigint(11) DEFAULT NULL,
  `area_sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `area_deep` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '地区深度，从1开始',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `area_parent_id` (`area_parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='地区表';

/*Data for the table `shop_area` */

/*Table structure for table `shop_article` */

DROP TABLE IF EXISTS `shop_article`;

CREATE TABLE `shop_article` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `ac_id` bigint(11) DEFAULT NULL,
  `article_url` varchar(100) DEFAULT NULL COMMENT '跳转链接',
  `article_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示，0为否，1为是，默认为1',
  `article_sort` tinyint(3) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  `article_title` varchar(100) DEFAULT NULL COMMENT '标题',
  `article_content` text COMMENT '内容',
  `is_del` tinyint(1) DEFAULT NULL COMMENT '0:未删除;1.已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `article_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ac_id` (`ac_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章表';

/*Data for the table `shop_article` */

/*Table structure for table `shop_article_class` */

DROP TABLE IF EXISTS `shop_article_class`;

CREATE TABLE `shop_article_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `ac_code` varchar(20) DEFAULT NULL COMMENT '分类标识码',
  `ac_name` varchar(100) NOT NULL COMMENT '分类名称',
  `ac_parent_id` bigint(11) DEFAULT NULL,
  `ac_sort` tinyint(1) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `ac_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '分类状态0:启用;1:停用',
  PRIMARY KEY (`id`),
  KEY `ac_parent_id` (`ac_parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章分类表';

/*Data for the table `shop_article_class` */

/*Table structure for table `shop_attach_picture` */

DROP TABLE IF EXISTS `shop_attach_picture`;

CREATE TABLE `shop_attach_picture` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `realName` varchar(225) DEFAULT NULL COMMENT '图片原名',
  `localName` varchar(225) DEFAULT NULL COMMENT '上传后的名字',
  `thumbnailPath` varchar(225) DEFAULT NULL COMMENT '缩略图路径',
  `localPath` varchar(255) DEFAULT NULL COMMENT '原图路径',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `storeid` int(11) DEFAULT NULL COMMENT '店铺id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图片库';

/*Data for the table `shop_attach_picture` */

/*Table structure for table `shop_attribute` */

DROP TABLE IF EXISTS `shop_attribute`;

CREATE TABLE `shop_attribute` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `attr_name` varchar(100) NOT NULL COMMENT '属性名称',
  `type_id` bigint(11) DEFAULT NULL,
  `attr_value` text NOT NULL COMMENT '属性值列',
  `attr_show` tinyint(1) unsigned NOT NULL COMMENT '是否显示。0为不显示、1为显示',
  `attr_sort` tinyint(1) unsigned NOT NULL COMMENT '排序',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '是否删除0:未删除；1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `attr_id` (`id`,`type_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品属性表';

/*Data for the table `shop_attribute` */

/*Table structure for table `shop_attribute_value` */

DROP TABLE IF EXISTS `shop_attribute_value`;

CREATE TABLE `shop_attribute_value` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `attr_value_name` varchar(100) NOT NULL COMMENT '属性值名称',
  `attr_id` bigint(11) DEFAULT NULL,
  `type_id` bigint(11) DEFAULT NULL,
  `attr_value_sort` tinyint(1) unsigned NOT NULL COMMENT '属性值排序',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品属性值表';

/*Data for the table `shop_attribute_value` */

/*Table structure for table `shop_barter` */

DROP TABLE IF EXISTS `shop_barter`;

CREATE TABLE `shop_barter` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `order_id` bigint(11) DEFAULT NULL,
  `order_sn` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barter_sn` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buyer_id` bigint(11) DEFAULT NULL,
  `buyer_name` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_id` bigint(11) DEFAULT NULL,
  `order_goods_id` bigint(11) DEFAULT NULL,
  `goods_name` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_num` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `goods_image` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_goods_type` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller_state` int(111) DEFAULT NULL,
  `order_lock` int(11) DEFAULT NULL,
  `goods_state` int(111) DEFAULT NULL,
  `create_time` bigint(111) DEFAULT NULL,
  `seller_time` bigint(111) DEFAULT NULL,
  `reason_id` bigint(11) DEFAULT NULL,
  `reason_info` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_info` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buyer_message` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller_message` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buyer_express_id` bigint(11) DEFAULT NULL,
  `buyer_invoice_no` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buyer_express_name` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buyer_ship_time` bigint(111) DEFAULT NULL,
  `buyer_delay_time` bigint(111) DEFAULT NULL,
  `buyer_receive_time` bigint(111) DEFAULT NULL,
  `buyer_receive_message` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller_express_id` bigint(11) DEFAULT NULL,
  `seller_invoice_no` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller_express_name` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seller_ship_time` bigint(111) DEFAULT NULL,
  `seller_delay_time` bigint(111) DEFAULT NULL,
  `seller_receive_message` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `shop_barter` */

/*Table structure for table `shop_barter_log` */

DROP TABLE IF EXISTS `shop_barter_log`;

CREATE TABLE `shop_barter_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `barter_id` bigint(11) DEFAULT NULL,
  `barter_state` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `change_state` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_info` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` bigint(111) DEFAULT NULL,
  `operator` varchar(111) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `shop_barter_log` */

/*Table structure for table `shop_brand` */

DROP TABLE IF EXISTS `shop_brand`;

CREATE TABLE `shop_brand` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `brand_name` varchar(100) DEFAULT NULL COMMENT '品牌名称',
  `brand_class` varchar(50) DEFAULT NULL COMMENT '类别名称',
  `brand_pic` varchar(100) DEFAULT NULL COMMENT '图片',
  `brand_sort` tinyint(3) unsigned DEFAULT '0' COMMENT '排序',
  `brand_recommend` tinyint(1) DEFAULT '0' COMMENT '推荐，0为否，1为是，默认为0',
  `store_id` bigint(11) DEFAULT NULL,
  `brand_apply` tinyint(1) NOT NULL DEFAULT '1' COMMENT '品牌申请，0为申请中，1为通过，默认为1，申请功能是会员使用，系统后台默认为1',
  `class_id` bigint(11) DEFAULT NULL,
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='品牌表';

/*Data for the table `shop_brand` */

/*Table structure for table `shop_cart` */

DROP TABLE IF EXISTS `shop_cart`;

CREATE TABLE `shop_cart` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `member_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(100) NOT NULL DEFAULT '0' COMMENT '店铺名称',
  `goods_id` bigint(11) DEFAULT NULL,
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `spec_id` bigint(11) DEFAULT NULL,
  `spec_info` text NOT NULL COMMENT '规格内容',
  `goods_store_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `goods_num` smallint(5) NOT NULL DEFAULT '1' COMMENT '购买商品数量',
  `goods_images` varchar(100) NOT NULL COMMENT '商品图片',
  `first_gc_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='购物车数据表';

/*Data for the table `shop_cart` */

/*Table structure for table `shop_complain` */

DROP TABLE IF EXISTS `shop_complain`;

CREATE TABLE `shop_complain` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `order_id` bigint(11) DEFAULT NULL,
  `accuser_id` bigint(11) DEFAULT NULL,
  `accuser_name` varchar(50) NOT NULL COMMENT '原告名称',
  `accused_id` bigint(11) DEFAULT NULL,
  `accused_name` varchar(50) NOT NULL COMMENT '被告名称',
  `complain_subject_content` varchar(50) NOT NULL COMMENT '投诉主题',
  `complain_subject_id` bigint(11) DEFAULT NULL,
  `complain_content` varchar(255) NOT NULL COMMENT '投诉内容',
  `complain_pic1` varchar(100) NOT NULL COMMENT '投诉图片1',
  `complain_pic2` varchar(100) NOT NULL COMMENT '投诉图片2',
  `complain_pic3` varchar(100) NOT NULL COMMENT '投诉图片3',
  `complain_datetime` bigint(13) DEFAULT NULL COMMENT '投诉时间',
  `complain_handle_datetime` bigint(13) DEFAULT NULL COMMENT '投诉处理时间',
  `complain_handle_member_id` bigint(11) DEFAULT NULL,
  `appeal_message` varchar(255) NOT NULL COMMENT '申诉内容',
  `appeal_datetime` bigint(13) DEFAULT NULL COMMENT '申诉时间',
  `appeal_pic1` varchar(100) NOT NULL COMMENT '申诉图片1',
  `appeal_pic2` varchar(100) NOT NULL COMMENT '申诉图片2',
  `appeal_pic3` varchar(100) NOT NULL COMMENT '申诉图片3',
  `final_handle_message` varchar(255) NOT NULL COMMENT '最终处理意见',
  `final_handle_datetime` bigint(13) DEFAULT NULL COMMENT '最终处理时间',
  `final_handle_member_id` bigint(11) DEFAULT NULL,
  `complain_state` tinyint(4) NOT NULL COMMENT '投诉状态(10-新投诉/20-投诉通过转给被投诉人/30-被投诉人已申诉/40-提交仲裁/99-已关闭)',
  `complain_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '投诉是否通过平台审批(1未通过/2通过)',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投诉表';

/*Data for the table `shop_complain` */

/*Table structure for table `shop_complain_goods` */

DROP TABLE IF EXISTS `shop_complain_goods`;

CREATE TABLE `shop_complain_goods` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `complain_id` bigint(11) DEFAULT NULL,
  `goods_id` bigint(11) DEFAULT NULL,
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `goods_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `goods_num` int(11) NOT NULL COMMENT '商品数量',
  `goods_image` varchar(100) NOT NULL DEFAULT '' COMMENT '商品图片',
  `complain_message` varchar(100) NOT NULL COMMENT '被投诉商品的问题描述',
  `order_goods_id` bigint(11) DEFAULT NULL,
  `order_goods_type` tinyint(1) unsigned DEFAULT '1' COMMENT '订单商品类型:1默认2团购商品3限时折扣商品4组合套装',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投诉商品表';

/*Data for the table `shop_complain_goods` */

/*Table structure for table `shop_complain_subject` */

DROP TABLE IF EXISTS `shop_complain_subject`;

CREATE TABLE `shop_complain_subject` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `complain_subject_content` varchar(50) NOT NULL COMMENT '投诉主题',
  `complain_subject_desc` varchar(100) NOT NULL COMMENT '投诉主题描述',
  `complain_subject_state` tinyint(4) NOT NULL COMMENT '投诉主题状态(1-有效/2-失效)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投诉主题表';

/*Data for the table `shop_complain_subject` */

/*Table structure for table `shop_complain_talk` */

DROP TABLE IF EXISTS `shop_complain_talk`;

CREATE TABLE `shop_complain_talk` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `complain_id` bigint(11) DEFAULT NULL,
  `talk_member_id` bigint(11) DEFAULT NULL,
  `talk_member_name` varchar(50) NOT NULL COMMENT '发言人名称',
  `talk_member_type` varchar(10) NOT NULL COMMENT '发言人类型(1-投诉人/2-被投诉人/3-平台)',
  `talk_content` varchar(255) NOT NULL COMMENT '发言内容',
  `talk_state` tinyint(4) NOT NULL COMMENT '发言状态(1-显示/2-不显示)',
  `talk_admin` int(11) NOT NULL DEFAULT '0' COMMENT '对话管理员，屏蔽对话人的id',
  `create_time` bigint(13) DEFAULT NULL COMMENT '对话发表时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投诉对话表';

/*Data for the table `shop_complain_talk` */

/*Table structure for table `shop_consult` */

DROP TABLE IF EXISTS `shop_consult`;

CREATE TABLE `shop_consult` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `goods_id` bigint(11) DEFAULT NULL,
  `cgoods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `member_id` bigint(11) DEFAULT NULL,
  `cmember_name` varchar(100) DEFAULT NULL COMMENT '会员名称',
  `store_id` bigint(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL COMMENT '咨询发布者邮箱',
  `consult_content` varchar(255) DEFAULT NULL COMMENT '咨询内容',
  `consult_addtime` bigint(13) DEFAULT NULL COMMENT '咨询添加时间',
  `consult_reply` varchar(255) DEFAULT NULL COMMENT '咨询回复内容',
  `consult_reply_time` bigint(13) DEFAULT NULL COMMENT '咨询回复时间',
  `isanonymous` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0表示不匿名 1表示匿名',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`) USING BTREE,
  KEY `seller_id` (`store_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品咨询表';

/*Data for the table `shop_consult` */

/*Table structure for table `shop_coupon` */

DROP TABLE IF EXISTS `shop_coupon`;

CREATE TABLE `shop_coupon` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `coupon_title` varchar(255) NOT NULL COMMENT '优惠券名称',
  `coupon_type` enum('1','2') DEFAULT NULL,
  `coupon_pic` varchar(255) NOT NULL,
  `coupon_desc` varchar(255) NOT NULL COMMENT '优惠券描述',
  `start_time` bigint(13) DEFAULT NULL COMMENT '优惠券开始日期',
  `end_time` bigint(13) DEFAULT NULL COMMENT '优惠券截止日期',
  `coupon_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠金额',
  `coupon_limit` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额限制',
  `store_id` bigint(11) DEFAULT NULL,
  `coupon_state` int(11) DEFAULT '1' COMMENT '上架与下架状态:1下架，0上架',
  `coupon_storage` int(11) NOT NULL DEFAULT '0' COMMENT '总共数量',
  `coupon_usage` int(11) NOT NULL DEFAULT '0' COMMENT '使用数量',
  `coupon_lock` int(11) NOT NULL DEFAULT '0' COMMENT '是否锁定：1锁定，0未锁定',
  `create_time` bigint(13) DEFAULT NULL COMMENT '添加日期',
  `coupon_class_id` bigint(11) DEFAULT NULL,
  `coupon_click` int(11) NOT NULL DEFAULT '1' COMMENT '点击次数',
  `coupon_print_style` varchar(255) DEFAULT '4STYLE' COMMENT '4STYLE STANDS FOR 4 COUPONS PER A4 PAGE, AND 8STYLE STANDS FOR 8 COUPONS PER A4 PAGE',
  `coupon_recommend` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0不推荐 1推荐到首页',
  `coupon_allowstate` int(1) unsigned DEFAULT '0' COMMENT '审核状态 0为待审核 1已通过 2未通过',
  `coupon_allowremark` varchar(255) DEFAULT NULL COMMENT '审核备注',
  `store_name` varchar(50) DEFAULT NULL COMMENT '店铺名称',
  `coupon_goods_id` bigint(11) DEFAULT NULL,
  `coupon_goods_class_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券';

/*Data for the table `shop_coupon` */

/*Table structure for table `shop_coupon_class` */

DROP TABLE IF EXISTS `shop_coupon_class`;

CREATE TABLE `shop_coupon_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `class_parent_id` bigint(11) DEFAULT NULL,
  `class_name` varchar(100) NOT NULL COMMENT '分类名称',
  `class_sort` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `class_show` int(11) NOT NULL DEFAULT '0' COMMENT '是否显示：0显示,1不显示',
  PRIMARY KEY (`id`),
  KEY `class_parent_id` (`class_parent_id`) USING BTREE,
  KEY `class_sort` (`class_sort`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='优惠券分类';

/*Data for the table `shop_coupon_class` */

/*Table structure for table `shop_coupon_member` */

DROP TABLE IF EXISTS `shop_coupon_member`;

CREATE TABLE `shop_coupon_member` (
  `id` bigint(11) DEFAULT NULL,
  `coupon_member_id` bigint(11) DEFAULT NULL,
  `coupon_isused` int(11) NOT NULL DEFAULT '0' COMMENT '是否使用:0没有使用，1已使用'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `shop_coupon_member` */

/*Table structure for table `shop_cron` */

DROP TABLE IF EXISTS `shop_cron`;

CREATE TABLE `shop_cron` (
  `id` bigint(11) DEFAULT NULL,
  `type` tinyint(3) unsigned DEFAULT NULL COMMENT '任务类型 1商品上架 2发送邮件 3优惠套装过期 4推荐展位过期',
  `exeid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '关联任务的ID[如商品ID,会员ID]',
  `exetime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行时间',
  `code` char(50) DEFAULT NULL COMMENT '邮件模板CODE',
  `content` text COMMENT '内容',
  KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务队列表';

/*Data for the table `shop_cron` */

/*Table structure for table `shop_daddress` */

DROP TABLE IF EXISTS `shop_daddress`;

CREATE TABLE `shop_daddress` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `store_id` bigint(11) DEFAULT NULL,
  `seller_name` varchar(50) NOT NULL DEFAULT '' COMMENT '联系人',
  `area_id` bigint(11) DEFAULT NULL,
  `city_id` bigint(11) DEFAULT NULL,
  `area_info` varchar(255) NOT NULL COMMENT '地区内容',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `zip_code` int(50) DEFAULT NULL COMMENT '邮编',
  `tel_phone` varchar(20) DEFAULT NULL COMMENT '座机电话',
  `mob_phone` varchar(15) DEFAULT NULL COMMENT '手机电话',
  `company` varchar(255) NOT NULL COMMENT '公司',
  `content` text COMMENT '备注',
  `is_default` enum('0','1') NOT NULL DEFAULT '0' COMMENT '是否默认1是',
  `province_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家发货地址信息表';

/*Data for the table `shop_daddress` */

/*Table structure for table `shop_dictionary` */

DROP TABLE IF EXISTS `shop_dictionary`;

CREATE TABLE `shop_dictionary` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `group_id` bigint(11) DEFAULT NULL,
  `dictionary_code` varchar(255) DEFAULT NULL COMMENT '字典编码',
  `dictionary_name` varchar(255) DEFAULT NULL COMMENT '字典名称',
  `dictionary_value` varchar(50) NOT NULL COMMENT '字典值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典表';

/*Data for the table `shop_dictionary` */

/*Table structure for table `shop_dictionary_group` */

DROP TABLE IF EXISTS `shop_dictionary_group`;

CREATE TABLE `shop_dictionary_group` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `group_code` varchar(255) DEFAULT NULL COMMENT '字典组编码',
  `group_name` varchar(255) DEFAULT NULL COMMENT '字典组名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据字典组表';

/*Data for the table `shop_dictionary_group` */

/*Table structure for table `shop_doc` */

DROP TABLE IF EXISTS `shop_doc`;

CREATE TABLE `shop_doc` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL COMMENT '标签',
  `tag_demo` varchar(1000) DEFAULT NULL COMMENT '标签试一试demo',
  `typeid` int(13) DEFAULT NULL COMMENT '分类名称',
  `api_address` varchar(255) DEFAULT NULL COMMENT 'api地址',
  `return_example_value` text COMMENT '返回示例',
  `pid` int(13) DEFAULT NULL COMMENT '父文档id',
  `sort` int(13) DEFAULT NULL COMMENT '排序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(13) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `createdby` varchar(255) DEFAULT NULL COMMENT '添加人',
  `updatecount` int(13) DEFAULT NULL COMMENT '修改次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `updateby` varchar(255) DEFAULT NULL COMMENT '修改人',
  `platform` varchar(255) DEFAULT NULL COMMENT '文档所属平台',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文档';

/*Data for the table `shop_doc` */

/*Table structure for table `shop_doc_entity` */

DROP TABLE IF EXISTS `shop_doc_entity`;

CREATE TABLE `shop_doc_entity` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL COMMENT '实体名',
  `code` varchar(255) DEFAULT NULL COMMENT '实体代码',
  `sort` int(13) DEFAULT NULL COMMENT '排序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(13) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `createdby` varchar(255) DEFAULT NULL COMMENT '添加人',
  `updatecount` int(13) DEFAULT NULL COMMENT '修改次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `updateby` varchar(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体';

/*Data for the table `shop_doc_entity` */

/*Table structure for table `shop_doc_entity_property` */

DROP TABLE IF EXISTS `shop_doc_entity_property`;

CREATE TABLE `shop_doc_entity_property` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL COMMENT '属性名称',
  `type` varchar(255) DEFAULT NULL COMMENT '数据类型',
  `entityid` int(13) DEFAULT NULL COMMENT '实体id',
  `sort` int(13) DEFAULT NULL COMMENT '排序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(13) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `createdby` varchar(255) DEFAULT NULL COMMENT '添加人',
  `updatecount` int(13) DEFAULT NULL COMMENT '修改次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `updateby` varchar(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体属性';

/*Data for the table `shop_doc_entity_property` */

/*Table structure for table `shop_doc_entity_ref` */

DROP TABLE IF EXISTS `shop_doc_entity_ref`;

CREATE TABLE `shop_doc_entity_ref` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `docid` int(13) DEFAULT NULL COMMENT '文档id',
  `entityid` int(13) DEFAULT NULL COMMENT '实体id',
  `sort` int(13) DEFAULT NULL COMMENT '排序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(13) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `createdby` varchar(255) DEFAULT NULL COMMENT '添加人',
  `updatecount` int(13) DEFAULT NULL COMMENT '修改次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `updateby` varchar(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='实体与文档关联表';

/*Data for the table `shop_doc_entity_ref` */

/*Table structure for table `shop_doc_param` */

DROP TABLE IF EXISTS `shop_doc_param`;

CREATE TABLE `shop_doc_param` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `docid` int(13) DEFAULT NULL COMMENT '文档id',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `type` int(13) DEFAULT NULL COMMENT '类型',
  `isrequired` varchar(255) DEFAULT NULL COMMENT '是否必须',
  `example_value` varchar(255) DEFAULT NULL COMMENT '示例值',
  `defalut_value` varchar(255) DEFAULT NULL COMMENT '默认值',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(13) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `createdby` varchar(255) DEFAULT NULL COMMENT '添加人',
  `updatecount` int(13) DEFAULT NULL COMMENT '修改次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `updateby` varchar(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='输入参数';

/*Data for the table `shop_doc_param` */

/*Table structure for table `shop_doc_returnvalue` */

DROP TABLE IF EXISTS `shop_doc_returnvalue`;

CREATE TABLE `shop_doc_returnvalue` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `docid` int(13) DEFAULT NULL COMMENT '文档id',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `type` varchar(255) DEFAULT NULL COMMENT '类型',
  `isrequired` int(13) DEFAULT NULL COMMENT '是否必须',
  `default_value` varchar(255) DEFAULT NULL COMMENT '默认值',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(13) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `createdby` varchar(255) DEFAULT NULL COMMENT '添加人',
  `updatecount` int(13) DEFAULT NULL COMMENT '修改次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `updateby` varchar(255) DEFAULT NULL COMMENT '修改人',
  `code` varchar(255) DEFAULT NULL COMMENT '实体代码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文档分类';

/*Data for the table `shop_doc_returnvalue` */

/*Table structure for table `shop_doc_type` */

DROP TABLE IF EXISTS `shop_doc_type`;

CREATE TABLE `shop_doc_type` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL COMMENT '文档分类名称',
  `sort` int(13) DEFAULT NULL COMMENT '排序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `status` int(13) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `createdby` varchar(255) DEFAULT NULL COMMENT '添加人',
  `updatecount` int(13) DEFAULT NULL COMMENT '修改次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `updateby` varchar(255) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文档分类';

/*Data for the table `shop_doc_type` */

/*Table structure for table `shop_document` */

DROP TABLE IF EXISTS `shop_document`;

CREATE TABLE `shop_document` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `doc_code` varchar(255) NOT NULL COMMENT '调用标识码',
  `doc_title` varchar(255) NOT NULL COMMENT '标题',
  `doc_content` text NOT NULL COMMENT '内容',
  `doc_time` datetime DEFAULT NULL COMMENT '添加时间/修改时间',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `is_del` tinyint(1) DEFAULT NULL COMMENT '0:未删除;1.已删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `doc_code` (`doc_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统文章表';

/*Data for the table `shop_document` */

/*Table structure for table `shop_evaluate_goods` */

DROP TABLE IF EXISTS `shop_evaluate_goods`;

CREATE TABLE `shop_evaluate_goods` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `geval_orderid` int(11) NOT NULL COMMENT '订单表自增ID',
  `geval_orderno` bigint(20) unsigned NOT NULL COMMENT '订单编号',
  `geval_ordergoodsid` int(11) NOT NULL COMMENT '订单商品表编号',
  `geval_goodsid` int(11) NOT NULL COMMENT '商品表编号',
  `geval_goodsname` varchar(100) NOT NULL COMMENT '商品名称',
  `geval_goodsprice` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `geval_scores` tinyint(1) NOT NULL COMMENT '1-5分',
  `geval_content` varchar(255) DEFAULT NULL COMMENT '信誉评价内容',
  `geval_isanonymous` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0表示不是 1表示是匿名评价',
  `geval_addtime` bigint(13) DEFAULT NULL COMMENT '评价时间',
  `geval_storeid` int(11) NOT NULL COMMENT '店铺编号',
  `geval_storename` varchar(100) NOT NULL COMMENT '店铺名称',
  `geval_frommemberid` int(11) NOT NULL COMMENT '评价人编号',
  `geval_frommembername` varchar(100) NOT NULL COMMENT '评价人名称',
  `geval_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '评价信息的状态 0为正常 1为禁止显示',
  `geval_remark` varchar(255) DEFAULT NULL COMMENT '管理员对评价的处理备注',
  `geval_explain` varchar(255) DEFAULT NULL COMMENT '解释内容',
  `geval_image` varchar(255) DEFAULT NULL COMMENT '晒单图片',
  `is_del` tinyint(1) DEFAULT NULL COMMENT '0:未删除;1.已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `spec_info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='信誉评价表';

/*Data for the table `shop_evaluate_goods` */

/*Table structure for table `shop_evaluate_store` */

DROP TABLE IF EXISTS `shop_evaluate_store`;

CREATE TABLE `shop_evaluate_store` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `seval_orderid` int(11) unsigned NOT NULL COMMENT '订单ID',
  `seval_orderno` varchar(100) NOT NULL COMMENT '订单编号',
  `seval_addtime` bigint(13) DEFAULT NULL COMMENT '评价时间',
  `seval_storeid` int(11) unsigned NOT NULL COMMENT '店铺编号',
  `seval_storename` varchar(100) NOT NULL COMMENT '店铺名称',
  `seval_memberid` int(11) unsigned NOT NULL COMMENT '买家编号',
  `seval_membername` varchar(100) NOT NULL COMMENT '买家名称',
  `seval_desccredit` double unsigned NOT NULL DEFAULT '5' COMMENT '描述相符评分',
  `seval_servicecredit` double unsigned NOT NULL DEFAULT '5' COMMENT '服务态度评分',
  `seval_deliverycredit` double unsigned NOT NULL DEFAULT '5' COMMENT '发货速度评分',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺评分表';

/*Data for the table `shop_evaluate_store` */

/*Table structure for table `shop_express` */

DROP TABLE IF EXISTS `shop_express`;

CREATE TABLE `shop_express` (
  `id` bigint(11) DEFAULT NULL,
  `e_name` varchar(50) NOT NULL COMMENT '公司名称',
  `e_state` int(4) NOT NULL DEFAULT '0' COMMENT '状态',
  `e_code` varchar(50) NOT NULL COMMENT '编号',
  `e_letter` char(1) NOT NULL COMMENT '首字母',
  `e_order` int(4) NOT NULL DEFAULT '0' COMMENT '1常用0不常用',
  `e_url` varchar(100) NOT NULL COMMENT '公司网址',
  `is_del` int(4) NOT NULL DEFAULT '0' COMMENT '删除标记',
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='快递公司';

/*Data for the table `shop_express` */

/*Table structure for table `shop_favorites` */

DROP TABLE IF EXISTS `shop_favorites`;

CREATE TABLE `shop_favorites` (
  `member_id` bigint(11) DEFAULT NULL,
  `fav_id` bigint(11) DEFAULT NULL,
  `fav_type` varchar(20) NOT NULL COMMENT '收藏类型',
  `fav_time` bigint(13) DEFAULT NULL COMMENT '收藏时间',
  `id` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='买家收藏表';

/*Data for the table `shop_favorites` */

/*Table structure for table `shop_feedback` */

DROP TABLE IF EXISTS `shop_feedback`;

CREATE TABLE `shop_feedback` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '标题',
  `content` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '内容',
  `email` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '电话',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `phone_type` int(11) DEFAULT NULL COMMENT 'IOS 1,ANDROID 2,other 3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shop_feedback` */

/*Table structure for table `shop_front_menu` */

DROP TABLE IF EXISTS `shop_front_menu`;

CREATE TABLE `shop_front_menu` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `m_name` varchar(50) NOT NULL COMMENT '菜单名',
  `m_url` varchar(255) NOT NULL COMMENT 'url路径',
  `m_parent_id` bigint(11) DEFAULT NULL,
  `m_sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `m_level` int(10) NOT NULL COMMENT '菜单等级',
  `m_path` varchar(255) NOT NULL COMMENT '菜单ID路径',
  `m_description` varchar(255) DEFAULT NULL COMMENT '描述',
  `is_Del` int(10) DEFAULT NULL COMMENT '是否能删除',
  `m_icon` varchar(20) DEFAULT NULL COMMENT '菜单显示图标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='前台菜单表';

/*Data for the table `shop_front_menu` */

/*Table structure for table `shop_gadmin` */

DROP TABLE IF EXISTS `shop_gadmin`;

CREATE TABLE `shop_gadmin` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `gname` varchar(50) DEFAULT NULL COMMENT '组名',
  `limits` text COMMENT '权限内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限组';

/*Data for the table `shop_gadmin` */

/*Table structure for table `shop_goods` */

DROP TABLE IF EXISTS `shop_goods`;

CREATE TABLE `shop_goods` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `goods_subtitle` varchar(200) DEFAULT NULL COMMENT '商品副标题',
  `gc_id` bigint(11) DEFAULT NULL,
  `gc_name` varchar(200) NOT NULL COMMENT '商品分类名称',
  `brand_id` bigint(11) DEFAULT NULL,
  `brand_name` varchar(100) DEFAULT NULL,
  `type_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(100) DEFAULT NULL,
  `spec_open` int(1) NOT NULL DEFAULT '0' COMMENT '商品规格开启状态，1开启，0关闭',
  `spec_id` bigint(11) DEFAULT NULL,
  `spec_name` longtext COMMENT '规格名称',
  `goods_image` varchar(100) DEFAULT NULL COMMENT '商品默认封面图片',
  `goods_image_more` text COMMENT '商品多图',
  `goods_store_price` decimal(10,2) NOT NULL COMMENT '商品店铺价格',
  `goods_serial` varchar(50) DEFAULT '' COMMENT '商品货号',
  `goods_show` int(1) NOT NULL COMMENT '商品上架1上架0下架2定时上架',
  `goods_click` int(11) NOT NULL DEFAULT '1' COMMENT '商品浏览数',
  `goods_state` int(1) NOT NULL DEFAULT '0' COMMENT '商品状态，0开启，1违规下架',
  `goods_commend` int(1) NOT NULL COMMENT '商品推荐',
  `create_time` bigint(13) DEFAULT NULL,
  `goods_keywords` varchar(255) DEFAULT '' COMMENT '商品关键字',
  `goods_description` varchar(255) DEFAULT '' COMMENT '商品描述',
  `goods_body` text COMMENT '商品详细内容',
  `goods_attr` text COMMENT '商品属性',
  `goods_spec` text COMMENT '商品规格',
  `goods_col_img` text COMMENT '颜色自定义图片',
  `update_time` bigint(13) DEFAULT NULL,
  `start_time` bigint(13) DEFAULT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `goods_form` int(1) unsigned NOT NULL DEFAULT '1' COMMENT '商品类型,1为全新、2为二手',
  `transport_id` bigint(11) DEFAULT NULL,
  `py_price` decimal(10,2) DEFAULT '0.00' COMMENT '平邮',
  `kd_price` decimal(10,2) DEFAULT '0.00' COMMENT '快递',
  `es_price` decimal(10,2) DEFAULT '0.00' COMMENT 'EMS',
  `city_id` bigint(11) DEFAULT NULL,
  `city_name` varchar(30) DEFAULT NULL,
  `province_id` bigint(11) DEFAULT NULL,
  `province_name` varchar(30) DEFAULT NULL,
  `goods_close_reason` varchar(255) DEFAULT NULL COMMENT '商品违规下架原因',
  `goods_store_state` int(1) NOT NULL DEFAULT '0' COMMENT '商品所在店铺状态 0开启 1关闭',
  `commentnum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '评论次数',
  `salenum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '售出数量',
  `goods_collect` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品收藏数量',
  `goods_transfee_charge` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '商品运费承担方式 默认 0为买家承担 1为卖家承担',
  `store_class_id` bigint(11) DEFAULT NULL,
  `is_del` int(2) NOT NULL DEFAULT '0' COMMENT '是否删除 0:未删除  1:已删除',
  `goods_market_price` decimal(10,2) DEFAULT NULL COMMENT '市场价',
  `goods_cost_price` decimal(10,2) DEFAULT NULL COMMENT '成本价',
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`) USING BTREE,
  KEY `gc_id` (`gc_id`) USING BTREE,
  KEY `goods_starttime` (`start_time`) USING BTREE,
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

/*Data for the table `shop_goods` */

/*Table structure for table `shop_goods_attr_index` */

DROP TABLE IF EXISTS `shop_goods_attr_index`;

CREATE TABLE `shop_goods_attr_index` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `gc_id` bigint(11) NOT NULL DEFAULT '0',
  `type_id` bigint(11) DEFAULT NULL,
  `attr_id` bigint(11) DEFAULT NULL,
  `attr_value_id` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`gc_id`,`attr_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品与属性对应表';

/*Data for the table `shop_goods_attr_index` */

/*Table structure for table `shop_goods_class` */

DROP TABLE IF EXISTS `shop_goods_class`;

CREATE TABLE `shop_goods_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `gc_name` varchar(100) NOT NULL COMMENT '分类名称',
  `gc_pic` varchar(255) DEFAULT NULL COMMENT '分类图片',
  `type_id` bigint(11) DEFAULT NULL,
  `type_name` varchar(100) NOT NULL COMMENT '类型名称',
  `gc_parent_id` bigint(11) DEFAULT NULL,
  `gc_sort` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `gc_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '前台显示，0为否，1为是，默认为1',
  `gc_title` varchar(200) DEFAULT NULL COMMENT '名称',
  `gc_keywords` varchar(255) DEFAULT '' COMMENT '关键词',
  `gc_description` varchar(255) DEFAULT '' COMMENT '描述',
  `gc_idpath` varchar(255) DEFAULT '' COMMENT '层级path',
  `expen_scale` float DEFAULT NULL COMMENT '费用比例',
  `is_relate` tinyint(1) DEFAULT NULL COMMENT '是否关联子分类 0否, 1是',
  PRIMARY KEY (`id`),
  KEY `store_id` (`gc_parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品分类表';

/*Data for the table `shop_goods_class` */

/*Table structure for table `shop_goods_class_staple` */

DROP TABLE IF EXISTS `shop_goods_class_staple`;

CREATE TABLE `shop_goods_class_staple` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `staple_name` varchar(255) NOT NULL COMMENT '常用分类名称',
  `gc_id_1` int(10) unsigned NOT NULL COMMENT '一级分类id',
  `gc_id_2` int(10) unsigned NOT NULL COMMENT '二级商品分类',
  `gc_id_3` int(10) unsigned NOT NULL COMMENT '三级商品分类',
  `type_id` bigint(11) DEFAULT NULL,
  `member_id` bigint(11) DEFAULT NULL,
  `counter` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '计数器',
  PRIMARY KEY (`id`),
  KEY `store_id` (`member_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺常用分类表';

/*Data for the table `shop_goods_class_staple` */

/*Table structure for table `shop_goods_class_tag` */

DROP TABLE IF EXISTS `shop_goods_class_tag`;

CREATE TABLE `shop_goods_class_tag` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `gc_id_1` int(10) unsigned NOT NULL COMMENT '一级分类id',
  `gc_id_2` int(10) unsigned NOT NULL COMMENT '二级分类id',
  `gc_id_3` int(10) unsigned NOT NULL COMMENT '三级分类id',
  `gc_tag_name` varchar(255) NOT NULL COMMENT '分类TAG名称',
  `gc_tag_value` text NOT NULL COMMENT '分类TAG值',
  `gc_id` bigint(11) DEFAULT NULL,
  `type_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品分类TAG表';

/*Data for the table `shop_goods_class_tag` */

/*Table structure for table `shop_goods_combination` */

DROP TABLE IF EXISTS `shop_goods_combination`;

CREATE TABLE `shop_goods_combination` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `combination_goods_id` bigint(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`combination_goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='组合商品表';

/*Data for the table `shop_goods_combination` */

/*Table structure for table `shop_goods_common` */

DROP TABLE IF EXISTS `shop_goods_common`;

CREATE TABLE `shop_goods_common` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `goods_name` varchar(50) NOT NULL COMMENT '商品名称',
  `goods_jingle` varchar(50) NOT NULL COMMENT '商品广告词',
  `gc_id` bigint(11) DEFAULT NULL,
  `gc_name` varchar(200) NOT NULL COMMENT '商品分类',
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `spec_name` varchar(255) NOT NULL COMMENT '规格名称',
  `spec_value` text NOT NULL COMMENT '规格值',
  `brand_id` bigint(11) DEFAULT NULL,
  `brand_name` varchar(100) NOT NULL COMMENT '品牌名称',
  `type_id` bigint(11) DEFAULT NULL,
  `goods_image` varchar(100) NOT NULL COMMENT '商品主图',
  `goods_attr` text NOT NULL COMMENT '商品属性',
  `goods_body` text NOT NULL COMMENT '商品内容',
  `goods_state` tinyint(3) unsigned NOT NULL COMMENT '商品状态 0下架，1正常，10违规（禁售）',
  `goods_stateremark` varchar(255) DEFAULT NULL COMMENT '违规原因',
  `goods_verify` tinyint(3) unsigned NOT NULL COMMENT '商品审核 1通过，0未通过，10审核中',
  `goods_verifyremark` varchar(255) DEFAULT NULL COMMENT '审核失败原因',
  `goods_lock` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品锁定 0未锁，1已锁',
  `goods_addtime` bigint(13) DEFAULT NULL COMMENT '商品添加时间',
  `goods_selltime` bigint(13) DEFAULT NULL COMMENT '上架时间',
  `goods_specname` text NOT NULL COMMENT '规格名称序列化（下标为规格id）',
  `goods_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `goods_marketprice` decimal(10,2) NOT NULL COMMENT '市场价',
  `goods_costprice` decimal(10,2) NOT NULL COMMENT '成本价',
  `goods_discount` float unsigned NOT NULL COMMENT '折扣',
  `goods_serial` varchar(50) NOT NULL COMMENT '商家编号',
  `transport_id` bigint(11) DEFAULT NULL,
  `transport_title` varchar(60) NOT NULL DEFAULT '' COMMENT '运费模板名称',
  `goods_commend` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品推荐 1是，0否，默认为0',
  `goods_freight` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '运费 0为免运费',
  `goods_vat` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否开具增值税发票 1是，0否',
  `areaid_1` int(10) unsigned NOT NULL COMMENT '一级地区id',
  `areaid_2` int(10) unsigned NOT NULL COMMENT '二级地区id',
  `goods_stcids` varchar(255) NOT NULL DEFAULT '' COMMENT '店铺分类id 首尾用,隔开',
  `plateid_top` int(10) unsigned DEFAULT NULL COMMENT '顶部关联板式',
  `plateid_bottom` int(10) unsigned DEFAULT NULL COMMENT '底部关联板式',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品公共内容表';

/*Data for the table `shop_goods_common` */

/*Table structure for table `shop_goods_images` */

DROP TABLE IF EXISTS `shop_goods_images`;

CREATE TABLE `shop_goods_images` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `goods_commonid` int(10) unsigned NOT NULL COMMENT '商品公共内容id',
  `store_id` bigint(11) DEFAULT NULL,
  `color_id` bigint(11) DEFAULT NULL,
  `goods_image` varchar(1000) NOT NULL COMMENT '商品图片',
  `goods_image_sort` tinyint(3) unsigned NOT NULL COMMENT '排序',
  `is_default` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '默认主题，1是，0否',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品图片';

/*Data for the table `shop_goods_images` */

/*Table structure for table `shop_goods_marking` */

DROP TABLE IF EXISTS `shop_goods_marking`;

CREATE TABLE `shop_goods_marking` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `startTime` bigint(20) DEFAULT NULL COMMENT '免邮开始时间',
  `endTime` bigint(20) DEFAULT NULL COMMENT '免邮结束时间',
  `startDate` bigint(20) DEFAULT NULL COMMENT '满送开始时间',
  `endDate` bigint(20) DEFAULT NULL COMMENT '满送结束时间',
  `goodsId` bigint(20) DEFAULT NULL,
  `storeId` bigint(20) DEFAULT NULL,
  `npstate` tinyint(4) DEFAULT NULL,
  `nojianstate` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `shop_goods_marking` */

/*Table structure for table `shop_goods_recommend` */

DROP TABLE IF EXISTS `shop_goods_recommend`;

CREATE TABLE `shop_goods_recommend` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `recommend_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '名字',
  `recommend_info` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '描述',
  `recommend_use` tinyint(1) DEFAULT NULL COMMENT '是否启用0启用,1停用',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shop_goods_recommend` */

/*Table structure for table `shop_goods_spec` */

DROP TABLE IF EXISTS `shop_goods_spec`;

CREATE TABLE `shop_goods_spec` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `goods_id` bigint(11) DEFAULT NULL,
  `spec_name` varchar(255) DEFAULT NULL COMMENT '规格名称',
  `spec_goods_price` decimal(10,2) DEFAULT NULL COMMENT '规格商品价格',
  `spec_goods_storage` int(11) NOT NULL COMMENT '规格商品库存',
  `spec_salenum` int(11) NOT NULL DEFAULT '0' COMMENT '售出数量',
  `spec_goods_color` varchar(20) DEFAULT NULL COMMENT '规格商品颜色',
  `spec_goods_serial` varchar(50) DEFAULT NULL COMMENT '规格商品编号',
  `spec_goods_spec` text COMMENT '商品规格序列化',
  `spec_isopen` int(1) DEFAULT '1' COMMENT '是否开启规格,1:开启，0:关闭',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品规格表';

/*Data for the table `shop_goods_spec` */

/*Table structure for table `shop_goods_spec_index` */

DROP TABLE IF EXISTS `shop_goods_spec_index`;

CREATE TABLE `shop_goods_spec_index` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `gc_id` bigint(11) NOT NULL DEFAULT '0',
  `type_id` bigint(11) DEFAULT NULL,
  `sp_id` bigint(11) DEFAULT NULL,
  `sp_value_id` bigint(11) NOT NULL DEFAULT '0',
  `sp_value_name` varchar(100) DEFAULT NULL COMMENT '规格值名称',
  PRIMARY KEY (`id`,`gc_id`,`sp_value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品与规格对应表';

/*Data for the table `shop_goods_spec_index` */

/*Table structure for table `shop_goods_words` */

DROP TABLE IF EXISTS `shop_goods_words`;

CREATE TABLE `shop_goods_words` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `keyword` varchar(100) DEFAULT NULL,
  `quanping` varchar(50) CHARACTER SET latin1 DEFAULT NULL COMMENT '全拼',
  `shouzimu` varchar(20) CHARACTER SET latin1 DEFAULT NULL COMMENT '关键词首字母',
  `words_num` int(11) DEFAULT NULL COMMENT '关键词出现次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品关键词表';

/*Data for the table `shop_goods_words` */

/*Table structure for table `shop_invoice` */

DROP TABLE IF EXISTS `shop_invoice`;

CREATE TABLE `shop_invoice` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `member_id` bigint(11) DEFAULT NULL,
  `inv_state` enum('1','2') DEFAULT NULL COMMENT '1普通发票2增值税发票',
  `inv_title` varchar(50) DEFAULT '' COMMENT '发票抬头[普通发票]',
  `inv_content` varchar(10) DEFAULT '' COMMENT '发票内容[普通发票]',
  `inv_company` varchar(50) DEFAULT '' COMMENT '单位名称',
  `inv_code` varchar(50) DEFAULT '' COMMENT '纳税人识别号',
  `inv_reg_addr` varchar(50) DEFAULT '' COMMENT '注册地址',
  `inv_reg_phone` varchar(30) DEFAULT '' COMMENT '注册电话',
  `inv_reg_bname` varchar(30) DEFAULT '' COMMENT '开户银行',
  `inv_reg_baccount` varchar(30) DEFAULT '' COMMENT '银行帐户',
  `inv_rec_name` varchar(20) DEFAULT '' COMMENT '收票人姓名',
  `inv_rec_mobphone` varchar(15) DEFAULT '' COMMENT '收票人手机号',
  `inv_rec_province` varchar(30) DEFAULT '' COMMENT '收票人省份',
  `inv_goto_addr` varchar(50) DEFAULT '' COMMENT '送票地址',
  `is_default` int(2) DEFAULT '0' COMMENT '是否是默认的',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='买家发票信息表';

/*Data for the table `shop_invoice` */

/*Table structure for table `shop_mail_msg_temlates` */

DROP TABLE IF EXISTS `shop_mail_msg_temlates`;

CREATE TABLE `shop_mail_msg_temlates` (
  `name` varchar(100) NOT NULL COMMENT '模板名称',
  `title` varchar(100) DEFAULT NULL COMMENT '模板标题',
  `code` varchar(100) NOT NULL COMMENT '模板调用代码',
  `content` text NOT NULL COMMENT '模板内容',
  `type` tinyint(1) NOT NULL COMMENT '模板类别，0为邮件，1为短信息，默认为0',
  `mail_switch` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否开启',
  `id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮件模板表';

/*Data for the table `shop_mail_msg_temlates` */

/*Table structure for table `shop_member` */

DROP TABLE IF EXISTS `shop_member`;

CREATE TABLE `shop_member` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `member_name` varchar(50) NOT NULL COMMENT '会员名称',
  `member_truename` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `member_avatar` varchar(300) DEFAULT NULL,
  `member_sex` int(4) DEFAULT '1' COMMENT '会员性别',
  `member_birthday` bigint(13) DEFAULT NULL COMMENT '生日',
  `member_passwd` varchar(300) DEFAULT NULL,
  `member_email` varchar(100) DEFAULT NULL COMMENT '会员邮箱',
  `member_qq` varchar(100) DEFAULT NULL COMMENT 'qq',
  `member_ww` varchar(100) DEFAULT NULL COMMENT '阿里旺旺',
  `member_login_num` int(11) DEFAULT '1' COMMENT '登录次数',
  `create_time` bigint(13) DEFAULT NULL COMMENT '会员注册时间',
  `member_login_time` bigint(13) DEFAULT NULL COMMENT '当前登录时间',
  `member_old_login_time` bigint(13) DEFAULT NULL COMMENT '上次登录时间',
  `member_login_ip` varchar(20) DEFAULT NULL COMMENT '当前登录ip',
  `member_old_login_ip` varchar(20) DEFAULT NULL COMMENT '上次登录ip',
  `member_openid` varchar(100) DEFAULT NULL,
  `member_info` text,
  `member_consume_points` int(11) DEFAULT NULL COMMENT '会员消费积分',
  `member_rank_points` int(11) DEFAULT NULL COMMENT '会员等级积分',
  `available_predeposit` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '预存款可用金额',
  `freeze_predeposit` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '预存款冻结金额',
  `inform_allow` tinyint(1) DEFAULT '1' COMMENT '是否允许举报(1可以/2不可以)',
  `is_buy` tinyint(1) DEFAULT '1' COMMENT '会员是否有购买权限 1为开启 0为关闭',
  `is_allowtalk` tinyint(1) DEFAULT '1' COMMENT '会员是否有咨询和发送站内信的权限 1为开启 0为关闭',
  `member_state` tinyint(1) DEFAULT '1' COMMENT '会员的开启状态 1为开启 0为关闭',
  `member_credit` int(11) DEFAULT '0' COMMENT '会员信用',
  `member_snsvisitnum` int(11) DEFAULT '0' COMMENT 'sns空间访问次数',
  `member_areaid` int(11) DEFAULT NULL COMMENT '地区ID',
  `member_cityid` int(11) DEFAULT NULL COMMENT '城市ID',
  `member_provinceid` int(11) DEFAULT NULL COMMENT '省份ID',
  `member_areainfo` varchar(255) DEFAULT NULL COMMENT '地区内容',
  `member_privacy` text COMMENT '隐私设定',
  `is_del` tinyint(1) DEFAULT '0' COMMENT '删除标志',
  `sign_code` varchar(100) DEFAULT NULL,
  `sign_code_state` varchar(100) DEFAULT NULL,
  `member_mobile` varchar(11) DEFAULT NULL COMMENT '手机号',
  `member_gradeid` int(11) DEFAULT NULL COMMENT '会员等级id',
  `member_type` varchar(11) DEFAULT NULL COMMENT '用户类型',
  PRIMARY KEY (`id`),
  KEY `member_name` (`member_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员表';

/*Data for the table `shop_member` */

/*Table structure for table `shop_member_grade` */

DROP TABLE IF EXISTS `shop_member_grade`;

CREATE TABLE `shop_member_grade` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `grade_name` varchar(10) CHARACTER SET utf8 DEFAULT NULL COMMENT '等级名称',
  `integration` int(11) DEFAULT NULL COMMENT '所需积分',
  `grade_img` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '等级所对应的图片',
  `preferential` int(11) DEFAULT NULL COMMENT '优惠百分比',
  `is_default` int(1) DEFAULT NULL COMMENT '是否是默认',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shop_member_grade` */

/*Table structure for table `shop_menu` */

DROP TABLE IF EXISTS `shop_menu`;

CREATE TABLE `shop_menu` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `m_name` varchar(50) NOT NULL COMMENT '菜单名',
  `m_url` varchar(255) NOT NULL COMMENT 'url路径',
  `m_parent_id` bigint(11) DEFAULT NULL,
  `m_sort` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `m_level` int(10) NOT NULL COMMENT '菜单等级',
  `m_path` varchar(255) NOT NULL COMMENT '菜单ID路径',
  `m_description` varchar(255) DEFAULT NULL COMMENT '描述',
  `m_permission` varchar(500) DEFAULT NULL COMMENT '权限标识',
  `m_isshow` int(3) DEFAULT '0' COMMENT '是否显示：0,否;1,是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='后台菜单表';

/*Data for the table `shop_menu` */

/*Table structure for table `shop_message` */

DROP TABLE IF EXISTS `shop_message`;

CREATE TABLE `shop_message` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `message_parent_id` bigint(11) DEFAULT NULL,
  `from_member_id` bigint(11) DEFAULT NULL,
  `to_member_id` bigint(11) DEFAULT NULL,
  `message_title` varchar(50) DEFAULT NULL COMMENT '短消息标题',
  `message_body` varchar(255) NOT NULL COMMENT '短消息内容',
  `message_time` bigint(13) DEFAULT NULL COMMENT '短消息发送时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '短消息回复更新时间',
  `message_open` tinyint(1) NOT NULL DEFAULT '0' COMMENT '短消息打开状态',
  `message_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '短消息状态，0为正常状态，1为发送人删除状态，2为接收人删除状态',
  `message_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为私信、1为系统消息、2为留言',
  `read_member_id` bigint(11) DEFAULT NULL,
  `del_member_id` bigint(11) DEFAULT NULL,
  `message_ismore` tinyint(1) NOT NULL DEFAULT '0' COMMENT '站内信是否为一条发给多个用户 0为否 1为多条 ',
  `from_member_name` varchar(100) DEFAULT NULL COMMENT '发信息人用户名',
  `to_member_name` varchar(100) DEFAULT NULL COMMENT '接收人用户名',
  PRIMARY KEY (`id`),
  KEY `from_member_id` (`from_member_id`) USING BTREE,
  KEY `to_member_id` (`to_member_id`) USING BTREE,
  KEY `message_ismore` (`message_ismore`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短消息';

/*Data for the table `shop_message` */

/*Table structure for table `shop_navigation` */

DROP TABLE IF EXISTS `shop_navigation`;

CREATE TABLE `shop_navigation` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `nav_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类别，0自定义导航，1商品分类，2文章导航，3活动导航，默认为0',
  `nav_title` varchar(100) DEFAULT NULL COMMENT '导航标题',
  `nav_url` varchar(255) DEFAULT NULL COMMENT '导航链接',
  `nav_location` tinyint(1) NOT NULL DEFAULT '0' COMMENT '导航位置，0头部，1中部，2底部，默认为0',
  `nav_new_open` tinyint(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '是否以新窗口打开，0为否，1为是，默认为0',
  `nav_sort` tinyint(3) unsigned NOT NULL DEFAULT '255' COMMENT '排序',
  `item_id` bigint(11) DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='页面导航表';

/*Data for the table `shop_navigation` */

/*Table structure for table `shop_offpay_area` */

DROP TABLE IF EXISTS `shop_offpay_area`;

CREATE TABLE `shop_offpay_area` (
  `id` bigint(11) DEFAULT NULL,
  `area_id` bigint(11) DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='货到付款支持地区表';

/*Data for the table `shop_offpay_area` */

/*Table structure for table `shop_order` */

DROP TABLE IF EXISTS `shop_order`;

CREATE TABLE `shop_order` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `order_sn` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(50) NOT NULL COMMENT '卖家店铺名称',
  `buyer_id` bigint(11) DEFAULT NULL,
  `buyer_name` varchar(50) NOT NULL COMMENT '买家姓名',
  `buyer_email` varchar(100) DEFAULT NULL COMMENT '买家电子邮箱',
  `create_time` bigint(13) DEFAULT NULL COMMENT '订单生成时间',
  `order_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '订单类型 0.普通 1.团购',
  `payment_id` bigint(11) DEFAULT NULL,
  `payment_name` varchar(50) NOT NULL COMMENT '支付方式名称',
  `payment_code` varchar(50) NOT NULL COMMENT '支付方式名称代码',
  `payment_branch` varchar(20) DEFAULT NULL COMMENT '支付分支',
  `payment_direct` char(1) DEFAULT '1' COMMENT '支付类型:1是即时到帐,2是担保交易',
  `payment_state` tinyint(1) DEFAULT '0' COMMENT '付款状态:0:未付款;1:已付款',
  `out_sn` varchar(100) DEFAULT NULL COMMENT '订单编号，外部支付时使用，有些外部支付系统要求特定的订单编号',
  `trade_sn` varchar(50) DEFAULT NULL COMMENT '交易流水号',
  `payment_time` bigint(13) DEFAULT NULL COMMENT '支付(付款)时间',
  `pay_message` text COMMENT '支付留言',
  `shipping_time` bigint(13) DEFAULT NULL COMMENT '配送时间',
  `shipping_express_id` bigint(11) DEFAULT NULL,
  `shipping_code` varchar(50) DEFAULT '' COMMENT '物流单号',
  `out_payment_code` varchar(255) DEFAULT NULL COMMENT '外部交易平台单独使用的标识字符串',
  `finnshed_time` bigint(13) DEFAULT NULL COMMENT '订单完成时间',
  `invoice` varchar(100) DEFAULT NULL COMMENT '发票信息',
  `goods_amount` decimal(10,2) NOT NULL COMMENT '商品总价格',
  `discount` decimal(10,2) DEFAULT NULL COMMENT '优惠总金额',
  `order_amount` decimal(10,2) DEFAULT NULL COMMENT '订单应付金额',
  `order_total_price` decimal(10,2) DEFAULT NULL COMMENT '订单总价格',
  `shipping_fee` decimal(10,2) DEFAULT NULL COMMENT '运费价格',
  `shipping_name` varchar(10) DEFAULT '' COMMENT '配送方式',
  `evaluation_status` tinyint(1) DEFAULT '0' COMMENT '评价状态 0为评价，1已评价',
  `evaluation_time` bigint(13) DEFAULT NULL COMMENT '评价时间',
  `evalseller_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '卖家是否已评价买家',
  `evalseller_time` bigint(13) DEFAULT NULL COMMENT '卖家评价买家的时间',
  `order_message` varchar(300) DEFAULT NULL COMMENT '订单留言',
  `order_state` int(11) NOT NULL DEFAULT '10' COMMENT '订单状态：0:已取消;10:待付款;20:待发货;30:待收货;40:交易完成;50:已提交;60:已确认;',
  `order_pointscount` int(11) NOT NULL DEFAULT '0' COMMENT '订单赠送积分',
  `voucher_id` bigint(11) DEFAULT NULL,
  `voucher_price` decimal(11,2) DEFAULT NULL COMMENT '代金券面额',
  `voucher_code` varchar(32) DEFAULT NULL COMMENT '代金券编码',
  `refund_state` tinyint(1) unsigned DEFAULT '0' COMMENT '退款状态:0是无退款,1是部分退款,2是全部退款',
  `return_state` tinyint(1) unsigned DEFAULT '0' COMMENT '退货状态:0是无退货,1是部分退货,2是全部退货',
  `refund_amount` decimal(10,2) DEFAULT '0.00' COMMENT '退款金额',
  `return_num` int(10) unsigned DEFAULT '0' COMMENT '退货数量',
  `group_id` bigint(11) DEFAULT NULL,
  `group_count` int(10) unsigned DEFAULT '0' COMMENT '团购数量',
  `xianshi_id` bigint(11) DEFAULT NULL,
  `xianshi_explain` varchar(100) DEFAULT '' COMMENT '限时折扣说明',
  `mansong_id` bigint(11) DEFAULT NULL,
  `mansong_explain` varchar(200) DEFAULT '' COMMENT '满就送说明',
  `bundling_id` bigint(11) DEFAULT NULL,
  `bundling_explain` varchar(100) DEFAULT NULL COMMENT '搭配套餐说明',
  `order_from` enum('1','2') DEFAULT '1' COMMENT '1PC2手机端',
  `deliver_explain` text COMMENT '发货备注',
  `daddress_id` bigint(11) DEFAULT NULL,
  `address_id` bigint(11) DEFAULT NULL,
  `pay_id` bigint(11) DEFAULT NULL,
  `pay_sn` varchar(50) DEFAULT NULL COMMENT '订单支付表编号',
  `balance_state` tinyint(1) DEFAULT '0' COMMENT '结算状态:0,未结算,1已结算',
  `balance_time` bigint(13) DEFAULT NULL COMMENT '结算时间',
  `shipping_express_code` varchar(50) DEFAULT '' COMMENT '配送公司编号',
  `predeposit_amount` decimal(10,2) NOT NULL COMMENT '余额支付金额',
  `cancel_cause` varchar(50) DEFAULT NULL COMMENT '订单取消原因',
  `coupon_id` bigint(11) DEFAULT NULL,
  `coupon_price` decimal(10,2) DEFAULT NULL COMMENT '优惠券金额',
  `promo_price` decimal(10,2) DEFAULT NULL COMMENT '促销金额',
  `lock_state` tinyint(1) DEFAULT '0' COMMENT '锁定状态:0是正常,大于0是锁定,默认是0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

/*Data for the table `shop_order` */

/*Table structure for table `shop_order_address` */

DROP TABLE IF EXISTS `shop_order_address`;

CREATE TABLE `shop_order_address` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `member_id` bigint(11) DEFAULT NULL,
  `true_name` varchar(50) NOT NULL COMMENT '会员姓名',
  `area_id` bigint(11) DEFAULT NULL,
  `city_id` bigint(11) DEFAULT NULL,
  `area_info` varchar(255) NOT NULL DEFAULT '' COMMENT '地区内容',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `tel_phone` varchar(20) DEFAULT NULL COMMENT '座机电话',
  `mob_phone` varchar(15) DEFAULT NULL COMMENT '手机电话',
  `is_default` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1默认收货地址',
  `province_id` bigint(11) DEFAULT NULL,
  `zip_code` int(50) DEFAULT NULL COMMENT '邮编',
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单地址信息表';

/*Data for the table `shop_order_address` */

/*Table structure for table `shop_order_bill` */

DROP TABLE IF EXISTS `shop_order_bill`;

CREATE TABLE `shop_order_bill` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `ob_no` varchar(30) NOT NULL COMMENT '结算单编号(年月店铺ID)',
  `ob_start_time` bigint(13) NOT NULL COMMENT '开始日期',
  `ob_end_time` bigint(13) NOT NULL COMMENT '结束日期',
  `ob_order_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `ob_shipping_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `ob_order_return_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '退单金额',
  `ob_commis_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '佣金金额',
  `ob_commis_return_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '退还佣金',
  `ob_store_cost_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '店铺促销活动费用',
  `ob_result_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '应结金额',
  `create_time` bigint(13) DEFAULT NULL COMMENT '生成结算单日期',
  `os_month` int(6) DEFAULT NULL COMMENT '结算单年月份',
  `os_year` int(6) DEFAULT NULL COMMENT '结算单年份',
  `ob_state` int(3) DEFAULT '10' COMMENT '10默认20店家已确认30平台已审核40结算完成',
  `ob_pay_time` bigint(13) DEFAULT NULL COMMENT '付款日期',
  `ob_pay_content` varchar(200) DEFAULT '' COMMENT '支付备注',
  `ob_store_id` bigint(11) DEFAULT NULL,
  `ob_store_name` varchar(50) DEFAULT NULL COMMENT '店铺名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算表';

/*Data for the table `shop_order_bill` */

/*Table structure for table `shop_order_common` */

DROP TABLE IF EXISTS `shop_order_common`;

CREATE TABLE `shop_order_common` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `store_id` bigint(11) DEFAULT NULL,
  `shipping_express_id` bigint(11) DEFAULT NULL,
  `evalseller_state` enum('0','1') NOT NULL DEFAULT '0' COMMENT '卖家是否已评价买家',
  `order_message` varchar(300) DEFAULT NULL COMMENT '订单留言',
  `order_pointscount` int(11) NOT NULL DEFAULT '0' COMMENT '订单赠送积分',
  `voucher_price` decimal(11,0) DEFAULT NULL COMMENT '代金券面额',
  `voucher_code` varchar(32) DEFAULT NULL COMMENT '代金券编码',
  `deliver_explain` varchar(255) DEFAULT NULL COMMENT '发货备注',
  `daddress_id` bigint(11) DEFAULT NULL,
  `reciver_name` varchar(50) NOT NULL COMMENT '收货人姓名',
  `reciver_info` varchar(500) NOT NULL COMMENT '收货人其它信息',
  `reciver_province_id` bigint(11) DEFAULT NULL,
  `invoice_info` varchar(500) DEFAULT '' COMMENT '发票信息',
  `promotion_info` varchar(500) DEFAULT '' COMMENT '促销信息备注',
  `shipping_time` bigint(13) DEFAULT NULL COMMENT '配送时间',
  `evaluation_time` bigint(13) DEFAULT NULL COMMENT '评价时间',
  `evalseller_time` bigint(13) DEFAULT NULL COMMENT '卖家评价买家的时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单信息扩展表';

/*Data for the table `shop_order_common` */

/*Table structure for table `shop_order_daddress` */

DROP TABLE IF EXISTS `shop_order_daddress`;

CREATE TABLE `shop_order_daddress` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `store_id` bigint(11) DEFAULT NULL,
  `seller_name` varchar(50) NOT NULL DEFAULT '' COMMENT '联系人',
  `area_id` bigint(11) DEFAULT NULL,
  `city_id` bigint(11) DEFAULT NULL,
  `area_info` varchar(255) NOT NULL COMMENT '地区内容',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `zip_code` int(50) DEFAULT NULL COMMENT '邮编',
  `tel_phone` varchar(20) DEFAULT NULL COMMENT '座机电话',
  `mob_phone` varchar(15) DEFAULT NULL COMMENT '手机电话',
  `company` varchar(255) NOT NULL COMMENT '公司',
  `content` text COMMENT '备注',
  `is_default` enum('0','1') NOT NULL DEFAULT '0' COMMENT '是否默认1是',
  `province_id` bigint(11) DEFAULT NULL,
  `order_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='订单发货地址信息表';

/*Data for the table `shop_order_daddress` */

/*Table structure for table `shop_order_goods` */

DROP TABLE IF EXISTS `shop_order_goods`;

CREATE TABLE `shop_order_goods` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `order_id` bigint(11) DEFAULT NULL,
  `goods_id` bigint(11) DEFAULT NULL,
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `spec_id` bigint(11) DEFAULT NULL,
  `spec_info` varchar(255) DEFAULT NULL COMMENT '规格描述',
  `goods_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  `goods_num` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `goods_image` varchar(100) DEFAULT NULL COMMENT '商品图片',
  `goods_returnnum` smallint(5) unsigned DEFAULT '0' COMMENT '退货数量',
  `stores_id` bigint(11) DEFAULT NULL,
  `evaluation_status` tinyint(1) DEFAULT '0' COMMENT '评价状态 0为评价，1已评价',
  `evaluation_time` bigint(13) DEFAULT NULL COMMENT '评价时间',
  `goods_pay_price` decimal(10,2) DEFAULT NULL COMMENT '商品实际成交价',
  `buyer_id` bigint(11) DEFAULT NULL,
  `commis_rate` float DEFAULT NULL COMMENT '佣金比例',
  `gc_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单商品表';

/*Data for the table `shop_order_goods` */

/*Table structure for table `shop_order_invoice` */

DROP TABLE IF EXISTS `shop_order_invoice`;

CREATE TABLE `shop_order_invoice` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `member_id` bigint(11) DEFAULT NULL,
  `order_id` bigint(11) DEFAULT NULL,
  `inv_state` enum('1','2') DEFAULT NULL COMMENT '1普通发票2增值税发票',
  `inv_title` varchar(50) DEFAULT '' COMMENT '发票抬头[普通发票]',
  `inv_content` varchar(10) DEFAULT '' COMMENT '发票内容[普通发票]',
  `inv_company` varchar(50) DEFAULT '' COMMENT '单位名称',
  `inv_code` varchar(50) DEFAULT '' COMMENT '纳税人识别号',
  `inv_reg_addr` varchar(50) DEFAULT '' COMMENT '注册地址',
  `inv_reg_phone` varchar(30) DEFAULT '' COMMENT '注册电话',
  `inv_reg_bname` varchar(30) DEFAULT '' COMMENT '开户银行',
  `inv_reg_baccount` varchar(30) DEFAULT '' COMMENT '银行帐户',
  `inv_rec_name` varchar(20) DEFAULT '' COMMENT '收票人姓名',
  `inv_rec_mobphone` varchar(15) DEFAULT '' COMMENT '收票人手机号',
  `inv_rec_province` varchar(30) DEFAULT '' COMMENT '收票人省份',
  `inv_goto_addr` varchar(50) DEFAULT '' COMMENT '送票地址',
  `is_default` int(2) DEFAULT '0' COMMENT '是否是默认的',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='买家发票信息表';

/*Data for the table `shop_order_invoice` */

/*Table structure for table `shop_order_log` */

DROP TABLE IF EXISTS `shop_order_log`;

CREATE TABLE `shop_order_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `order_id` bigint(11) DEFAULT NULL,
  `order_state` varchar(20) NOT NULL COMMENT '订单状态信息',
  `change_state` varchar(20) NOT NULL COMMENT '下一步订单状态信息',
  `state_info` varchar(20) NOT NULL COMMENT '订单状态描述',
  `create_time` bigint(13) DEFAULT NULL COMMENT '处理时间',
  `operator` varchar(30) NOT NULL COMMENT '操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单处理历史表';

/*Data for the table `shop_order_log` */

/*Table structure for table `shop_order_pay` */

DROP TABLE IF EXISTS `shop_order_pay`;

CREATE TABLE `shop_order_pay` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `pay_sn` varchar(50) DEFAULT NULL COMMENT '支付单号',
  `buyer_id` bigint(11) DEFAULT NULL,
  `api_pay_state` enum('0','1') DEFAULT '0' COMMENT '0默认未支付1已支付(只有第三方支付接口通知到时才会更改此状态)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单支付表';

/*Data for the table `shop_order_pay` */

/*Table structure for table `shop_order_statis` */

DROP TABLE IF EXISTS `shop_order_statis`;

CREATE TABLE `shop_order_statis` (
  `os_month` mediumint(9) unsigned NOT NULL DEFAULT '0' COMMENT '统计编号(年月)',
  `os_year` smallint(6) DEFAULT '0' COMMENT '年',
  `os_start_date` bigint(13) DEFAULT NULL COMMENT '开始日期',
  `os_end_date` bigint(13) DEFAULT NULL COMMENT '结束日期',
  `os_order_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `os_shipping_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `os_order_return_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '退单金额',
  `os_commis_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '佣金金额',
  `os_commis_return_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '退还佣金',
  `os_store_cost_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '店铺促销活动费用',
  `os_result_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '本期应结',
  `os_create_date` bigint(13) DEFAULT NULL COMMENT '创建记录日期',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`os_month`),
  KEY `os_month` (`os_month`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='月销量统计表';

/*Data for the table `shop_order_statis` */

/*Table structure for table `shop_p_mansong` */

DROP TABLE IF EXISTS `shop_p_mansong`;

CREATE TABLE `shop_p_mansong` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `mansong_name` varchar(50) NOT NULL COMMENT '活动名称',
  `quota_id` bigint(11) DEFAULT NULL,
  `start_time` bigint(13) unsigned NOT NULL COMMENT '活动开始时间',
  `end_time` bigint(13) unsigned NOT NULL COMMENT '活动结束时间',
  `member_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `member_name` varchar(50) NOT NULL COMMENT '用户名',
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `state` tinyint(1) unsigned NOT NULL COMMENT '状态(1-新申请/2-审核通过/3-已取消/4-审核失败)',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='满就送活动表';

/*Data for the table `shop_p_mansong` */

/*Table structure for table `shop_p_mansong_quota` */

DROP TABLE IF EXISTS `shop_p_mansong_quota`;

CREATE TABLE `shop_p_mansong_quota` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `apply_id` bigint(11) DEFAULT NULL,
  `member_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `member_name` varchar(50) NOT NULL COMMENT '用户名',
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `start_time` bigint(13) unsigned NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) unsigned NOT NULL COMMENT '结束时间',
  `state` tinyint(1) unsigned DEFAULT NULL COMMENT '配额状态(1-可用/2-取消/3-结束)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='满就送套餐表';

/*Data for the table `shop_p_mansong_quota` */

/*Table structure for table `shop_p_mansong_rule` */

DROP TABLE IF EXISTS `shop_p_mansong_rule`;

CREATE TABLE `shop_p_mansong_rule` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `mansong_id` bigint(11) DEFAULT NULL,
  `level` tinyint(1) unsigned DEFAULT NULL COMMENT '规则级别(1/2/3)',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '级别价格',
  `shipping_free` tinyint(1) unsigned DEFAULT NULL COMMENT '免邮标志(0-不免邮/1-免邮费)',
  `discount` decimal(10,2) unsigned NOT NULL COMMENT '减现金优惠金额',
  `gift_name` varchar(50) DEFAULT NULL COMMENT '礼品名称',
  `gift_link` varchar(100) DEFAULT NULL COMMENT '礼品链接',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='满就送活动规则表';

/*Data for the table `shop_p_mansong_rule` */

/*Table structure for table `shop_payment` */

DROP TABLE IF EXISTS `shop_payment`;

CREATE TABLE `shop_payment` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `payment_code` char(10) NOT NULL COMMENT '支付代码名称',
  `payment_name` char(10) NOT NULL COMMENT '支付名称',
  `payment_config` text COMMENT '支付接口配置信息',
  `payment_state` enum('0','1') DEFAULT NULL COMMENT '接口状态0禁用1启用',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `payment_logo` varchar(55) DEFAULT NULL COMMENT '支付方式logo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付方式表';

/*Data for the table `shop_payment` */

/*Table structure for table `shop_pd_cash` */

DROP TABLE IF EXISTS `shop_pd_cash`;

CREATE TABLE `shop_pd_cash` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `pdc_sn` bigint(20) NOT NULL COMMENT '记录唯一标示',
  `pdc_member_id` bigint(11) DEFAULT NULL,
  `pdc_member_name` varchar(50) NOT NULL COMMENT '会员名称',
  `pdc_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `pdc_bank_name` varchar(40) NOT NULL COMMENT '收款银行',
  `pdc_bank_no` varchar(30) DEFAULT NULL COMMENT '收款账号',
  `pdc_bank_user` varchar(10) DEFAULT NULL COMMENT '开户人姓名',
  `create_time` int(11) NOT NULL COMMENT '添加时间',
  `pdc_payment_time` int(11) DEFAULT NULL COMMENT '付款时间',
  `pdc_payment_state` enum('0','1') NOT NULL DEFAULT '0' COMMENT '提现支付状态 0默认1支付完成',
  `pdc_payment_admin` varchar(30) DEFAULT NULL COMMENT '支付管理员',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预存款提现记录表';

/*Data for the table `shop_pd_cash` */

/*Table structure for table `shop_pd_log` */

DROP TABLE IF EXISTS `shop_pd_log`;

CREATE TABLE `shop_pd_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `lg_member_id` bigint(11) DEFAULT NULL,
  `lg_member_name` varchar(50) NOT NULL COMMENT '会员名称',
  `lg_admin_name` varchar(50) DEFAULT NULL COMMENT '管理员名称',
  `lg_type` varchar(15) NOT NULL DEFAULT '' COMMENT 'order_pay下单支付预存款,order_freeze下单冻结预存款,order_cancel取消订单解冻预存款,order_comb_pay下单支付被冻结的预存款,recharge充值,cash_apply申请提现冻结预存款,cash_pay提现成功,cash_del取消提现申请，解冻预存款,refund退款',
  `lg_av_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '可用金额变更0表示未变更',
  `lg_freeze_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '冻结金额变更0表示未变更',
  `create_time` bigint(13) DEFAULT NULL COMMENT '添加时间',
  `lg_desc` varchar(150) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预存款变更日志表';

/*Data for the table `shop_pd_log` */

/*Table structure for table `shop_pd_recharge` */

DROP TABLE IF EXISTS `shop_pd_recharge`;

CREATE TABLE `shop_pd_recharge` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `pdr_sn` varchar(50) NOT NULL COMMENT '记录唯一标示',
  `pdr_member_id` bigint(11) DEFAULT NULL,
  `pdr_member_name` varchar(50) NOT NULL COMMENT '会员名称',
  `pdr_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `pdr_payment_code` varchar(20) DEFAULT '' COMMENT '支付方式',
  `pdr_payment_name` varchar(15) DEFAULT '' COMMENT '支付方式',
  `pdr_trade_sn` varchar(50) DEFAULT '' COMMENT '第三方支付接口交易号',
  `create_time` bigint(13) DEFAULT NULL COMMENT '添加时间',
  `pdr_payment_state` enum('0','1') NOT NULL DEFAULT '0' COMMENT '支付状态 0未支付1支付',
  `pdr_payment_time` bigint(13) DEFAULT NULL COMMENT '支付时间',
  `pdr_admin` varchar(30) DEFAULT '' COMMENT '管理员名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预存款充值表';

/*Data for the table `shop_pd_recharge` */

/*Table structure for table `shop_points_goods` */

DROP TABLE IF EXISTS `shop_points_goods`;

CREATE TABLE `shop_points_goods` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `goods_spec_id` bigint(11) DEFAULT NULL,
  `points_goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `points_goods_subtitle` varchar(200) DEFAULT NULL COMMENT '商品副标题',
  `gc_id` bigint(11) DEFAULT NULL,
  `gc_name` varchar(200) NOT NULL COMMENT '商品分类名称',
  `brand_id` bigint(11) DEFAULT NULL,
  `brand_name` varchar(100) DEFAULT NULL,
  `type_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(100) DEFAULT NULL,
  `points_goods_image` varchar(100) DEFAULT NULL COMMENT '商品默认封面图片',
  `points_goods_image_more` text COMMENT '商品多图',
  `points_goods_store_price` decimal(10,2) NOT NULL COMMENT '商品店铺价格',
  `points_goods_serial` varchar(50) DEFAULT '' COMMENT '商品货号',
  `points_goods_show` int(1) NOT NULL COMMENT '商品上架1上架0下架2定时上架',
  `points_goods_click` int(11) NOT NULL DEFAULT '1' COMMENT '商品浏览数',
  `points_goods_commend` int(1) NOT NULL DEFAULT '0' COMMENT '商品推荐',
  `points_goods_add_time` bigint(13) DEFAULT NULL COMMENT '商品添加时间',
  `points_goods_body` text COMMENT '商品详细内容',
  `points_goods_attr` text COMMENT '商品属性',
  `points_goods_spec` text COMMENT '商品规格',
  `points_goods_starttime` bigint(13) DEFAULT NULL COMMENT '兑换开始时间',
  `points_goods_endtime` bigint(13) DEFAULT NULL COMMENT '兑换结束时间',
  `city_id` bigint(11) DEFAULT NULL,
  `city_name` varchar(30) DEFAULT NULL,
  `province_id` bigint(11) DEFAULT NULL,
  `province_name` varchar(30) DEFAULT NULL,
  `salenum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '售出数量',
  `points_goods_collect` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品收藏数量',
  `points_goods_transfee_charge` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '商品运费承担方式 默认 0为买家承担 1为卖家承担',
  `is_del` int(2) NOT NULL DEFAULT '0' COMMENT '是否删除 0:未删除  1:已删除',
  `pointsnums` int(11) NOT NULL COMMENT '所需积分',
  `member_grade_id` bigint(11) DEFAULT NULL,
  `exchange_count` int(11) DEFAULT NULL COMMENT '每个会员可以兑换该商品的数量',
  `points_goods_storage` int(11) NOT NULL COMMENT '库存',
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`) USING BTREE,
  KEY `gc_id` (`gc_id`) USING BTREE,
  KEY `goods_starttime` (`points_goods_starttime`) USING BTREE,
  KEY `brand_id` (`brand_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分商品表';

/*Data for the table `shop_points_goods` */

/*Table structure for table `shop_points_log` */

DROP TABLE IF EXISTS `shop_points_log`;

CREATE TABLE `shop_points_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `pl_memberid` int(11) NOT NULL COMMENT '会员编号',
  `pl_membername` varchar(100) NOT NULL COMMENT '会员名称',
  `pl_adminid` int(11) DEFAULT NULL COMMENT '管理员编号',
  `pl_adminname` varchar(100) DEFAULT NULL COMMENT '管理员名称',
  `pl_points` int(11) NOT NULL DEFAULT '0' COMMENT '积分数负数表示扣除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '添加时间',
  `pl_desc` varchar(100) NOT NULL COMMENT '操作描述',
  `pl_stage` varchar(50) NOT NULL COMMENT '操作阶段',
  `pl_type` int(3) DEFAULT NULL COMMENT '积分操作类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员积分日志表';

/*Data for the table `shop_points_log` */

/*Table structure for table `shop_promotion` */

DROP TABLE IF EXISTS `shop_promotion`;

CREATE TABLE `shop_promotion` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `p_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '优惠名称',
  `p_start_value` decimal(6,2) DEFAULT NULL COMMENT '优惠起始值',
  `p_promote_value` decimal(6,2) DEFAULT NULL COMMENT '优惠值',
  `pc_id` bigint(11) DEFAULT NULL,
  `p_status` int(1) DEFAULT NULL COMMENT '优惠状态',
  `p_sort` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shop_promotion` */

/*Table structure for table `shop_promotion_class` */

DROP TABLE IF EXISTS `shop_promotion_class`;

CREATE TABLE `shop_promotion_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `pc_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '分类名称',
  `pc_sort` int(4) DEFAULT NULL COMMENT '排序',
  `pc_start_time` bigint(13) DEFAULT NULL COMMENT '开始时间',
  `pc_end_time` bigint(13) DEFAULT NULL COMMENT '结束日期',
  `pc_status` int(1) DEFAULT NULL COMMENT '状态',
  `create_time` bigint(13) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shop_promotion_class` */

/*Table structure for table `shop_rec_position` */

DROP TABLE IF EXISTS `shop_rec_position`;

CREATE TABLE `shop_rec_position` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `pic_type` enum('1','2','0') NOT NULL DEFAULT '1' COMMENT '0文字1本地图片2远程',
  `title` varchar(200) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '序列化推荐位内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推荐位';

/*Data for the table `shop_rec_position` */

/*Table structure for table `shop_refund_log` */

DROP TABLE IF EXISTS `shop_refund_log`;

CREATE TABLE `shop_refund_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `order_id` bigint(11) DEFAULT NULL,
  `refund_sn` varchar(100) NOT NULL COMMENT '退款编号',
  `order_sn` varchar(100) NOT NULL COMMENT '订单编号',
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(20) NOT NULL COMMENT '店铺名称',
  `buyer_id` bigint(11) DEFAULT NULL,
  `buyer_name` varchar(50) NOT NULL COMMENT '买家会员名',
  `order_amount` decimal(10,2) NOT NULL COMMENT '订单金额',
  `order_refund` decimal(10,2) NOT NULL COMMENT '退款金额',
  `refund_paymentname` varchar(50) NOT NULL COMMENT '支付方式名称',
  `refund_paymentcode` varchar(50) NOT NULL COMMENT '支付方式代码',
  `refund_message` varchar(300) DEFAULT NULL COMMENT '退款备注',
  `buyer_message` varchar(300) DEFAULT NULL COMMENT '退款原因',
  `admin_message` varchar(300) DEFAULT NULL COMMENT '管理员处理原因',
  `seller_time` bigint(13) DEFAULT NULL COMMENT '卖家处理时间',
  `admin_time` bigint(13) DEFAULT NULL COMMENT '管理员处理时间',
  `confirm_time` bigint(13) DEFAULT NULL COMMENT '买家确认收款时间',
  `refund_type` tinyint(1) unsigned DEFAULT '2' COMMENT '类型:1为买家,2为卖家,默认为2',
  `refund_state` tinyint(1) unsigned DEFAULT '2' COMMENT '状态:1为待处理,2为同意,3为拒绝,默认为2',
  `buyer_confirm` tinyint(1) unsigned DEFAULT '1' COMMENT '确认收款状态:1为待确认,2为已确认,默认为2',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款表';

/*Data for the table `shop_refund_log` */

/*Table structure for table `shop_refund_reason` */

DROP TABLE IF EXISTS `shop_refund_reason`;

CREATE TABLE `shop_refund_reason` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `reason_info` varchar(50) NOT NULL COMMENT '原因内容',
  `sort` tinyint(1) unsigned DEFAULT '255' COMMENT '排序',
  `update_time` bigint(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款退货原因表';

/*Data for the table `shop_refund_reason` */

/*Table structure for table `shop_refund_return` */

DROP TABLE IF EXISTS `shop_refund_return`;

CREATE TABLE `shop_refund_return` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `order_id` bigint(11) DEFAULT NULL,
  `order_sn` varchar(50) NOT NULL COMMENT '订单编号',
  `refund_sn` varchar(50) NOT NULL COMMENT '申请编号',
  `store_id` bigint(11) DEFAULT NULL,
  `store_name` varchar(20) NOT NULL COMMENT '店铺名称',
  `buyer_id` bigint(11) DEFAULT NULL,
  `buyer_name` varchar(50) NOT NULL COMMENT '买家会员名',
  `goods_id` bigint(11) DEFAULT NULL,
  `order_goods_id` bigint(11) DEFAULT NULL,
  `goods_name` varchar(50) NOT NULL COMMENT '商品名称',
  `goods_num` int(10) unsigned DEFAULT '1' COMMENT '商品数量',
  `refund_amount` decimal(10,2) DEFAULT '0.00' COMMENT '退款金额',
  `goods_image` varchar(100) DEFAULT NULL COMMENT '商品图片',
  `order_goods_type` tinyint(1) unsigned DEFAULT '1' COMMENT '订单商品类型:1默认2团购商品3限时折扣商品4组合套装',
  `refund_type` tinyint(1) unsigned DEFAULT NULL COMMENT '申请类型:1为退款,2为退货',
  `seller_state` tinyint(1) unsigned DEFAULT NULL COMMENT '卖家处理状态:1为待审核,2为同意,3为不同意',
  `refund_state` tinyint(1) unsigned DEFAULT NULL COMMENT '申请状态:1为处理中,2为待管理员处理,3为已完成',
  `return_type` tinyint(1) unsigned DEFAULT NULL COMMENT '退货类型:1为不用退货,2为需要退货',
  `order_lock` tinyint(1) unsigned DEFAULT NULL COMMENT '订单锁定类型:1为不用锁定,2为需要锁定',
  `goods_state` tinyint(1) unsigned DEFAULT NULL COMMENT '物流状态:1为待发货,2为待收货,3为未收到,4为已收货',
  `create_time` bigint(13) unsigned NOT NULL COMMENT '添加时间',
  `seller_time` bigint(13) unsigned DEFAULT NULL COMMENT '卖家处理时间',
  `admin_time` bigint(13) unsigned DEFAULT NULL COMMENT '管理员处理时间',
  `reason_id` bigint(11) DEFAULT NULL,
  `reason_info` varchar(300) DEFAULT '' COMMENT '原因内容',
  `pic_info` varchar(300) DEFAULT '' COMMENT '图片',
  `buyer_message` varchar(300) DEFAULT NULL COMMENT '申请原因',
  `seller_message` varchar(300) DEFAULT NULL COMMENT '卖家备注',
  `admin_message` varchar(300) DEFAULT NULL COMMENT '管理员备注',
  `express_id` bigint(11) DEFAULT NULL,
  `express_name` varchar(50) DEFAULT NULL COMMENT '物流公司名称',
  `invoice_no` varchar(50) DEFAULT NULL COMMENT '物流单号',
  `ship_time` bigint(13) unsigned DEFAULT NULL COMMENT '发货时间',
  `delay_time` bigint(13) unsigned DEFAULT NULL COMMENT '收货延迟时间',
  `receive_time` bigint(13) unsigned DEFAULT NULL COMMENT '收货时间',
  `receive_message` varchar(300) DEFAULT NULL COMMENT '收货备注',
  `commis_rate` smallint(6) DEFAULT '0' COMMENT '佣金比例',
  `batch_no` varchar(50) DEFAULT NULL COMMENT '退款批次号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退款退货表';

/*Data for the table `shop_refund_return` */

/*Table structure for table `shop_rel_goods_recommend` */

DROP TABLE IF EXISTS `shop_rel_goods_recommend`;

CREATE TABLE `shop_rel_goods_recommend` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `recommend_id` bigint(11) DEFAULT NULL,
  `goods_id` bigint(11) DEFAULT NULL,
  `sort` int(4) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shop_rel_goods_recommend` */

/*Table structure for table `shop_return_log` */

DROP TABLE IF EXISTS `shop_return_log`;

CREATE TABLE `shop_return_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `return_id` bigint(11) DEFAULT NULL,
  `return_state` varchar(20) NOT NULL COMMENT '退货状态信息',
  `change_state` varchar(20) NOT NULL COMMENT '下一步退货状态信息',
  `state_info` varchar(255) NOT NULL COMMENT '退货状态描述',
  `create_time` bigint(13) DEFAULT NULL COMMENT '处理时间',
  `operator` varchar(30) NOT NULL COMMENT '操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='退货处理历史表';

/*Data for the table `shop_return_log` */

/*Table structure for table `shop_role` */

DROP TABLE IF EXISTS `shop_role`;

CREATE TABLE `shop_role` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `r_name` varchar(50) NOT NULL COMMENT '角色名称',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `r_description` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `r_alias` varchar(50) DEFAULT NULL COMMENT '角色别名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='平台后台角色';

/*Data for the table `shop_role` */

/*Table structure for table `shop_role_menu` */

DROP TABLE IF EXISTS `shop_role_menu`;

CREATE TABLE `shop_role_menu` (
  `id` bigint(11) DEFAULT NULL,
  `menu_id` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色菜单表';

/*Data for the table `shop_role_menu` */

/*Table structure for table `shop_salenum` */

DROP TABLE IF EXISTS `shop_salenum`;

CREATE TABLE `shop_salenum` (
  `date` int(8) unsigned NOT NULL COMMENT '销售日期',
  `salenum` int(11) unsigned NOT NULL COMMENT '销量',
  `goods_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='销量统计表';

/*Data for the table `shop_salenum` */

/*Table structure for table `shop_seller` */

DROP TABLE IF EXISTS `shop_seller`;

CREATE TABLE `shop_seller` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `seller_name` varchar(50) NOT NULL COMMENT '卖家用户名',
  `member_id` bigint(11) DEFAULT NULL,
  `seller_group_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `is_admin` tinyint(3) unsigned NOT NULL COMMENT '是否管理员(0-不是 1-是)',
  `seller_quicklink` varchar(255) DEFAULT NULL COMMENT '卖家快捷操作',
  `last_login_time` bigint(13) DEFAULT NULL COMMENT '最后登录时间',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家用户表';

/*Data for the table `shop_seller` */

/*Table structure for table `shop_seller_group` */

DROP TABLE IF EXISTS `shop_seller_group`;

CREATE TABLE `shop_seller_group` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `group_name` varchar(50) NOT NULL COMMENT '组名',
  `limits` text NOT NULL COMMENT '权限',
  `store_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家用户组表';

/*Data for the table `shop_seller_group` */

/*Table structure for table `shop_seller_log` */

DROP TABLE IF EXISTS `shop_seller_log`;

CREATE TABLE `shop_seller_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `log_content` varchar(50) NOT NULL COMMENT '日志内容',
  `create_time` bigint(13) DEFAULT NULL COMMENT '日志生成时间',
  `log_seller_id` bigint(11) DEFAULT NULL,
  `log_seller_name` varchar(50) NOT NULL COMMENT '卖家帐号',
  `log_store_id` bigint(11) DEFAULT NULL,
  `log_seller_ip` varchar(50) NOT NULL COMMENT '卖家ip',
  `log_url` varchar(50) NOT NULL COMMENT '日志url',
  `log_state` tinyint(3) unsigned NOT NULL COMMENT '日志状态(0-失败 1-成功)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家日志表';

/*Data for the table `shop_seller_log` */

/*Table structure for table `shop_seo` */

DROP TABLE IF EXISTS `shop_seo`;

CREATE TABLE `shop_seo` (
  `id` bigint(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL COMMENT '标题',
  `keywords` varchar(255) NOT NULL COMMENT '关键词',
  `description` text NOT NULL COMMENT '描述',
  `type` varchar(20) NOT NULL COMMENT '类型',
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SEO信息存放表';

/*Data for the table `shop_seo` */

/*Table structure for table `shop_setting` */

DROP TABLE IF EXISTS `shop_setting`;

CREATE TABLE `shop_setting` (
  `name` varchar(50) NOT NULL COMMENT '名称',
  `value` text COMMENT '值',
  `id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统设置表';

/*Data for the table `shop_setting` */

/*Table structure for table `shop_spec` */

DROP TABLE IF EXISTS `shop_spec`;

CREATE TABLE `shop_spec` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `sp_name` varchar(100) NOT NULL COMMENT '规格名称',
  `sp_format` int(1) NOT NULL COMMENT '0:text; 1:image',
  `sp_value` text NOT NULL COMMENT '规格值列',
  `sp_sort` tinyint(1) unsigned NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品规格表';

/*Data for the table `shop_spec` */

/*Table structure for table `shop_spec_value` */

DROP TABLE IF EXISTS `shop_spec_value`;

CREATE TABLE `shop_spec_value` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `sp_value_name` varchar(100) NOT NULL COMMENT '规格值名称',
  `sp_id` bigint(11) DEFAULT NULL,
  `sp_value_image` varchar(100) DEFAULT NULL COMMENT '规格图片',
  `sp_value_sort` tinyint(1) unsigned NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品规格值表';

/*Data for the table `shop_spec_value` */

/*Table structure for table `shop_stat_goods` */

DROP TABLE IF EXISTS `shop_stat_goods`;

CREATE TABLE `shop_stat_goods` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `goods_name` varchar(99) DEFAULT NULL COMMENT '商品名称',
  `goods_id` bigint(11) DEFAULT NULL,
  `member_name` varchar(19) DEFAULT NULL COMMENT '会员名称',
  `member_id` bigint(11) DEFAULT NULL,
  `login_ip` varchar(19) DEFAULT NULL COMMENT '登陆者ip',
  `login_time` bigint(13) DEFAULT NULL COMMENT '登陆时间',
  `store_id` bigint(11) DEFAULT NULL,
  `province` varchar(19) DEFAULT NULL COMMENT '省',
  `city` varchar(19) DEFAULT NULL,
  `area` varchar(19) DEFAULT NULL COMMENT '区',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品浏览日志表';

/*Data for the table `shop_stat_goods` */

/*Table structure for table `shop_stat_member` */

DROP TABLE IF EXISTS `shop_stat_member`;

CREATE TABLE `shop_stat_member` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `statm_memberid` int(11) NOT NULL COMMENT '会员ID',
  `statm_membername` varchar(100) NOT NULL COMMENT '会员名称',
  `statm_time` int(11) NOT NULL COMMENT '统计时间，当前按照最小时间单位为天',
  `statm_ordernum` int(11) NOT NULL DEFAULT '0' COMMENT '下单量',
  `statm_orderamount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '下单金额',
  `statm_goodsnum` int(11) NOT NULL DEFAULT '0' COMMENT '下单商品件数',
  `statm_predincrease` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '预存款增加额',
  `statm_predreduce` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '预存款减少额',
  `statm_pointsincrease` int(11) NOT NULL DEFAULT '0' COMMENT '积分增加额',
  `statm_pointsreduce` int(11) NOT NULL DEFAULT '0' COMMENT '积分减少额',
  `update_time` bigint(13) DEFAULT NULL COMMENT '记录更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员相关数据统计';

/*Data for the table `shop_stat_member` */

/*Table structure for table `shop_store` */

DROP TABLE IF EXISTS `shop_store`;

CREATE TABLE `shop_store` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `store_auth` int(1) DEFAULT '0' COMMENT '店铺认证,0-未认证，1-认证',
  `name_auth` int(1) NOT NULL DEFAULT '0' COMMENT '店主认证，0-未认证，1-认证',
  `grade_id` bigint(11) DEFAULT NULL,
  `member_id` bigint(11) DEFAULT NULL,
  `member_name` varchar(50) NOT NULL COMMENT '会员名称',
  `store_owner_card` varchar(50) DEFAULT NULL COMMENT '身份证',
  `sc_id` bigint(11) DEFAULT NULL,
  `area_id` bigint(11) DEFAULT NULL,
  `area_info` varchar(100) DEFAULT NULL COMMENT '地区内容，冗余数据',
  `store_address` varchar(100) DEFAULT NULL COMMENT '详细地区',
  `store_zip` varchar(10) DEFAULT NULL COMMENT '邮政编码',
  `store_tel` varchar(50) DEFAULT NULL COMMENT '电话号码',
  `store_sms` varchar(200) DEFAULT '' COMMENT '短信接口字段',
  `store_image` varchar(100) DEFAULT NULL COMMENT '证件上传',
  `store_image1` varchar(100) DEFAULT NULL COMMENT '执照上传',
  `store_state` int(1) DEFAULT '2' COMMENT '店铺状态，0关闭，1开启，2审核中',
  `store_close_info` varchar(255) DEFAULT NULL COMMENT '店铺关闭原因',
  `store_sort` int(11) DEFAULT '0' COMMENT '店铺排序',
  `store_time` bigint(13) DEFAULT NULL COMMENT '店铺时间',
  `end_time` bigint(13) DEFAULT NULL,
  `store_label` varchar(255) DEFAULT NULL COMMENT '店铺logo',
  `store_banner` varchar(255) DEFAULT NULL COMMENT '店铺横幅',
  `store_logo` varchar(255) DEFAULT NULL COMMENT '店标',
  `store_keywords` varchar(255) DEFAULT '' COMMENT '店铺seo关键字',
  `store_description` varchar(255) DEFAULT '' COMMENT '店铺seo描述',
  `store_qq` varchar(50) DEFAULT NULL COMMENT 'QQ',
  `store_ww` varchar(50) DEFAULT NULL COMMENT '阿里旺旺',
  `description` text COMMENT '店铺简介',
  `store_zy` text COMMENT '主营商品',
  `store_domain` varchar(50) DEFAULT NULL COMMENT '店铺二级域名',
  `store_domain_times` int(1) unsigned DEFAULT '0' COMMENT '二级域名修改次数',
  `store_recommend` int(1) DEFAULT '0' COMMENT '推荐，0为否，1为是，默认为0',
  `store_theme` varchar(50) DEFAULT 'default' COMMENT '店铺当前主题',
  `store_credit` int(10) DEFAULT '0' COMMENT '店铺信用',
  `praise_rate` float DEFAULT '0' COMMENT '店铺好评率',
  `store_desccredit` float DEFAULT '0' COMMENT '描述相符度分数',
  `store_servicecredit` float DEFAULT '0' COMMENT '服务态度分数',
  `store_deliverycredit` float DEFAULT '0' COMMENT '发货速度分数',
  `store_code` varchar(255) DEFAULT 'default_qrcode.png' COMMENT '店铺二维码',
  `store_collect` int(10) unsigned DEFAULT '0' COMMENT '店铺收藏数量',
  `store_slide` text COMMENT '店铺幻灯片',
  `store_slide_url` text COMMENT '店铺幻灯片链接',
  `store_center_quicklink` text COMMENT '卖家中心的常用操作快捷链接',
  `store_stamp` varchar(200) DEFAULT NULL COMMENT '店铺印章',
  `store_printdesc` varchar(500) DEFAULT NULL COMMENT '打印订单页面下方说明文字',
  `store_sales` int(10) unsigned DEFAULT '0' COMMENT '店铺销量',
  `store_presales` text COMMENT '售前客服',
  `store_aftersales` text COMMENT '售后客服',
  `store_workingtime` bigint(13) DEFAULT NULL COMMENT '工作时间',
  `city_id` bigint(11) DEFAULT NULL,
  `province_id` bigint(11) DEFAULT NULL,
  `store_click` int(11) DEFAULT '0' COMMENT '店铺点击量',
  `storecreate_time` bigint(13) DEFAULT NULL,
  `store_logintime` bigint(13) DEFAULT NULL COMMENT '当前登陆时间',
  `store_lastlogintime` bigint(13) DEFAULT NULL COMMENT '上次登陆时间',
  `store_longitude` varchar(20) DEFAULT NULL COMMENT '经度',
  `store_atitude` varchar(20) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`),
  KEY `store_name` (`store_name`) USING BTREE,
  KEY `sc_id` (`sc_id`) USING BTREE,
  KEY `area_id` (`area_id`) USING BTREE,
  KEY `store_state` (`store_state`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺数据表';

/*Data for the table `shop_store` */

/*Table structure for table `shop_store_bind_class` */

DROP TABLE IF EXISTS `shop_store_bind_class`;

CREATE TABLE `shop_store_bind_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `store_id` bigint(11) DEFAULT NULL,
  `commis_rate` tinyint(4) unsigned DEFAULT '0' COMMENT '佣金比例',
  `class_1` mediumint(9) unsigned DEFAULT '0' COMMENT '一级分类',
  `class_2` mediumint(9) unsigned DEFAULT '0' COMMENT '二级分类',
  `class_3` mediumint(9) unsigned DEFAULT '0' COMMENT '三级分类',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `store_id` (`store_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺可发布商品类目表';

/*Data for the table `shop_store_bind_class` */

/*Table structure for table `shop_store_class` */

DROP TABLE IF EXISTS `shop_store_class`;

CREATE TABLE `shop_store_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL COMMENT '分类名称',
  `parent_id` bigint(11) DEFAULT NULL,
  `sort` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `margin` int(19) DEFAULT NULL COMMENT '保证金',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `sc_parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺分类表';

/*Data for the table `shop_store_class` */

/*Table structure for table `shop_store_cost` */

DROP TABLE IF EXISTS `shop_store_cost`;

CREATE TABLE `shop_store_cost` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `cost_store_id` bigint(11) DEFAULT NULL,
  `cost_seller_id` bigint(11) DEFAULT NULL,
  `cost_price` int(10) unsigned NOT NULL COMMENT '价格',
  `cost_remark` varchar(255) NOT NULL COMMENT '费用备注',
  `cost_state` tinyint(3) unsigned NOT NULL COMMENT '费用状态(0-未结算 1-已结算)',
  `create_time` bigint(13) DEFAULT NULL COMMENT '费用发生时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺费用表';

/*Data for the table `shop_store_cost` */

/*Table structure for table `shop_store_extend` */

DROP TABLE IF EXISTS `shop_store_extend`;

CREATE TABLE `shop_store_extend` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `express` text COMMENT '快递公司ID的组合',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺信息扩展表';

/*Data for the table `shop_store_extend` */

/*Table structure for table `shop_store_goods_class` */

DROP TABLE IF EXISTS `shop_store_goods_class`;

CREATE TABLE `shop_store_goods_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `stc_name` varchar(50) NOT NULL COMMENT '店铺商品分类名称',
  `stc_parent_id` bigint(11) DEFAULT NULL,
  `stc_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '店铺商品分类状态',
  `store_id` bigint(11) DEFAULT NULL,
  `stc_sort` int(11) NOT NULL DEFAULT '0' COMMENT '商品分类排序',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `check_state` int(9) DEFAULT NULL COMMENT '是否审核通过，0审核中，1审核通过，2审核不通过',
  `reason` varchar(290) DEFAULT NULL COMMENT '审核结果',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `stc_parent_id` (`stc_parent_id`,`stc_sort`) USING BTREE,
  KEY `store_id` (`store_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺商品分类表';

/*Data for the table `shop_store_goods_class` */

/*Table structure for table `shop_store_grade` */

DROP TABLE IF EXISTS `shop_store_grade`;

CREATE TABLE `shop_store_grade` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `sg_name` char(50) DEFAULT NULL COMMENT '等级名称',
  `sg_goods_limit` mediumint(10) unsigned DEFAULT '0' COMMENT '允许发布的商品数量',
  `sg_album_limit` mediumint(8) unsigned DEFAULT '0' COMMENT '允许上传图片数量',
  `sg_space_limit` int(10) unsigned DEFAULT '0' COMMENT '上传空间大小，单位MB',
  `sg_template_number` tinyint(3) unsigned DEFAULT '0' COMMENT '选择店铺模板套数',
  `sg_template` varchar(255) DEFAULT NULL COMMENT '模板内容',
  `sg_price` varchar(100) DEFAULT NULL COMMENT '费用',
  `sg_confirm` tinyint(1) NOT NULL DEFAULT '1' COMMENT '审核，0为否，1为是，默认为1',
  `sg_description` text COMMENT '申请说明',
  `sg_function` varchar(255) DEFAULT NULL COMMENT '附加功能',
  `sg_sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '级别，数目越大级别越高',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `brokerage_scale` float DEFAULT NULL COMMENT '佣金比例',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺等级表';

/*Data for the table `shop_store_grade` */

/*Table structure for table `shop_store_joinin` */

DROP TABLE IF EXISTS `shop_store_joinin`;

CREATE TABLE `shop_store_joinin` (
  `member_id` bigint(11) NOT NULL DEFAULT '0',
  `member_name` varchar(50) DEFAULT NULL COMMENT '店主用户名',
  `company_name` varchar(50) DEFAULT NULL COMMENT '公司名称',
  `company_address` varchar(50) DEFAULT NULL COMMENT '公司地址',
  `company_address_detail` varchar(50) DEFAULT NULL COMMENT '公司详细地址',
  `company_phone` varchar(20) DEFAULT NULL COMMENT '公司电话',
  `company_employee_count` int(10) unsigned DEFAULT NULL COMMENT '员工总数',
  `company_registered_capital` int(10) unsigned DEFAULT NULL COMMENT '注册资金',
  `contacts_name` varchar(50) DEFAULT NULL COMMENT '联系人姓名',
  `contacts_phone` varchar(20) DEFAULT NULL COMMENT '联系人电话',
  `contacts_email` varchar(50) DEFAULT NULL COMMENT '联系人邮箱',
  `business_licence_number` varchar(50) DEFAULT NULL COMMENT '营业执照号',
  `business_licence_address` varchar(50) DEFAULT NULL COMMENT '营业执所在地',
  `business_licence_start` datetime DEFAULT NULL COMMENT '营业执照有效期开始',
  `business_licence_end` datetime DEFAULT NULL COMMENT '营业执照有效期结束',
  `business_sphere` varchar(1000) DEFAULT NULL COMMENT '法定经营范围',
  `business_licence_number_electronic` varchar(50) DEFAULT NULL COMMENT '营业执照电子版',
  `organization_code` varchar(50) DEFAULT NULL COMMENT '组织机构代码',
  `organization_code_electronic` varchar(50) DEFAULT NULL COMMENT '组织机构代码电子版',
  `general_taxpayer` varchar(50) DEFAULT NULL COMMENT '一般纳税人证明',
  `bank_account_name` varchar(50) DEFAULT NULL COMMENT '银行开户名',
  `bank_account_number` varchar(50) DEFAULT NULL COMMENT '公司银行账号',
  `bank_name` varchar(50) DEFAULT NULL COMMENT '开户银行支行名称',
  `bank_code` varchar(50) DEFAULT NULL COMMENT '支行联行号',
  `bank_address` varchar(50) DEFAULT NULL COMMENT '开户银行所在地',
  `bank_licence_electronic` varchar(50) DEFAULT NULL COMMENT '开户银行许可证电子版',
  `is_settlement_account` tinyint(1) DEFAULT NULL COMMENT '开户行账号是否为结算账号 1-开户行就是结算账号 2-独立的计算账号',
  `settlement_bank_account_name` varchar(50) DEFAULT NULL COMMENT '结算银行开户名',
  `settlement_bank_account_number` varchar(50) DEFAULT NULL COMMENT '结算公司银行账号',
  `settlement_bank_name` varchar(50) DEFAULT NULL COMMENT '结算开户银行支行名称',
  `settlement_bank_code` varchar(50) DEFAULT NULL COMMENT '结算支行联行号',
  `settlement_bank_address` varchar(50) DEFAULT NULL COMMENT '结算开户银行所在地',
  `tax_registration_certificate` varchar(50) DEFAULT NULL COMMENT '税务登记证号',
  `taxpayer_id` bigint(11) DEFAULT NULL,
  `tax_registration_certificate_electronic` varchar(50) DEFAULT NULL COMMENT '税务登记证号电子版',
  `seller_name` varchar(50) DEFAULT NULL COMMENT '卖家帐号',
  `store_name` varchar(50) DEFAULT NULL COMMENT '店铺名称',
  `store_class_ids` varchar(1000) DEFAULT NULL COMMENT '店铺分类编号集合',
  `store_class_names` varchar(1000) DEFAULT NULL COMMENT '店铺分类名称集合',
  `joinin_state` varchar(50) DEFAULT NULL COMMENT '申请状态 10-已提交申请 11-缴费完成  20-审核成功 30-审核失败 31-缴费审核失败 40-审核通过开店',
  `joinin_message` varchar(200) DEFAULT NULL COMMENT '管理员审核信息',
  `sg_name` varchar(50) DEFAULT NULL COMMENT '店铺等级名称',
  `sg_id` bigint(11) DEFAULT NULL,
  `sc_name` varchar(50) DEFAULT NULL COMMENT '店铺分类名称',
  `sc_id` bigint(11) DEFAULT NULL,
  `store_class_commis_rates` varchar(200) DEFAULT NULL COMMENT '分类佣金比例',
  `paying_money_certificate` varchar(50) DEFAULT NULL COMMENT '付款凭证',
  `paying_money_certificate_explain` varchar(200) DEFAULT NULL COMMENT '付款凭证说明',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺入住表';

/*Data for the table `shop_store_joinin` */

/*Table structure for table `shop_store_navigation` */

DROP TABLE IF EXISTS `shop_store_navigation`;

CREATE TABLE `shop_store_navigation` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `sn_title` varchar(50) NOT NULL COMMENT '导航名称',
  `sn_store_id` bigint(11) DEFAULT NULL,
  `sn_content` text COMMENT '导航内容',
  `sn_sort` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '导航排序',
  `sn_if_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '导航是否显示',
  `create_time` bigint(13) DEFAULT NULL COMMENT '导航添加时间',
  `sn_url` varchar(255) DEFAULT NULL COMMENT '店铺导航的外链URL',
  `sn_new_open` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '店铺导航外链是否在新窗口打开：0不开新窗口1开新窗口，默认是0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卖家店铺导航信息表';

/*Data for the table `shop_store_navigation` */

/*Table structure for table `shop_store_plate` */

DROP TABLE IF EXISTS `shop_store_plate`;

CREATE TABLE `shop_store_plate` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `plate_name` varchar(10) NOT NULL COMMENT '关联板式名称',
  `plate_position` tinyint(3) unsigned NOT NULL COMMENT '关联板式位置 1顶部，0底部',
  `plate_content` text NOT NULL COMMENT '关联板式内容',
  `store_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='关联板式表';

/*Data for the table `shop_store_plate` */

/*Table structure for table `shop_store_search` */

DROP TABLE IF EXISTS `shop_store_search`;

CREATE TABLE `shop_store_search` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `store_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registered_capital` int(30) DEFAULT NULL,
  `store_credit` int(2) DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_label` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `shop_store_search` */

/*Table structure for table `shop_store_sns_comment` */

DROP TABLE IF EXISTS `shop_store_sns_comment`;

CREATE TABLE `shop_store_sns_comment` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `strace_id` bigint(11) DEFAULT NULL,
  `scomm_content` varchar(150) DEFAULT NULL COMMENT '评论内容',
  `scomm_memberid` int(11) DEFAULT NULL COMMENT '会员id',
  `scomm_membername` varchar(45) DEFAULT NULL COMMENT '会员名称',
  `scomm_memberavatar` varchar(50) DEFAULT NULL COMMENT '会员头像',
  `scomm_time` varchar(11) DEFAULT NULL COMMENT '评论时间',
  `scomm_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '评论状态 1正常，0屏蔽',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `scomm_id` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺动态评论表';

/*Data for the table `shop_store_sns_comment` */

/*Table structure for table `shop_store_sns_setting` */

DROP TABLE IF EXISTS `shop_store_sns_setting`;

CREATE TABLE `shop_store_sns_setting` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `sauto_new` tinyint(4) NOT NULL DEFAULT '1' COMMENT '新品,0为关闭/1为开启',
  `sauto_newtitle` varchar(150) NOT NULL COMMENT '新品内容',
  `sauto_coupon` tinyint(4) NOT NULL DEFAULT '1' COMMENT '优惠券,0为关闭/1为开启',
  `sauto_coupontitle` varchar(150) NOT NULL COMMENT '优惠券内容',
  `sauto_xianshi` tinyint(4) NOT NULL DEFAULT '1' COMMENT '限时折扣,0为关闭/1为开启',
  `sauto_xianshititle` varchar(150) NOT NULL COMMENT '限时折扣内容',
  `sauto_mansong` tinyint(4) NOT NULL DEFAULT '1' COMMENT '满即送,0为关闭/1为开启',
  `sauto_mansongtitle` varchar(150) NOT NULL COMMENT '满即送内容',
  `sauto_bundling` tinyint(4) NOT NULL DEFAULT '1' COMMENT '组合销售,0为关闭/1为开启',
  `sauto_bundlingtitle` varchar(150) NOT NULL COMMENT '组合销售内容',
  `sauto_groupbuy` tinyint(4) NOT NULL DEFAULT '1' COMMENT '团购,0为关闭/1为开启',
  `sauto_groupbuytitle` varchar(150) NOT NULL COMMENT '团购内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺自动发布动态设置表';

/*Data for the table `shop_store_sns_setting` */

/*Table structure for table `shop_store_sns_tracelog` */

DROP TABLE IF EXISTS `shop_store_sns_tracelog`;

CREATE TABLE `shop_store_sns_tracelog` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `strace_storeid` int(11) DEFAULT NULL COMMENT '店铺id',
  `strace_storename` varchar(100) DEFAULT NULL COMMENT '店铺名称',
  `strace_storelogo` varchar(255) NOT NULL COMMENT '店标',
  `strace_title` varchar(150) DEFAULT NULL COMMENT '动态标题',
  `strace_content` text COMMENT '发表内容',
  `strace_time` bigint(13) DEFAULT NULL COMMENT '发表时间',
  `strace_cool` int(11) DEFAULT '0' COMMENT '赞数量',
  `strace_spread` int(11) DEFAULT '0' COMMENT '转播数量',
  `strace_comment` int(11) DEFAULT '0' COMMENT '评论数量',
  `strace_type` tinyint(4) DEFAULT '1' COMMENT '1=relay,2=normal,3=new,4=coupon,5=xianshi,6=mansong,7=bundling,8=groupbuy,9=recommend,10=hotsell',
  `strace_goodsdata` varchar(1000) DEFAULT NULL COMMENT '商品信息',
  `strace_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '动态状态 1正常，0屏蔽',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺动态表';

/*Data for the table `shop_store_sns_tracelog` */

/*Table structure for table `shop_store_watermark` */

DROP TABLE IF EXISTS `shop_store_watermark`;

CREATE TABLE `shop_store_watermark` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `jpeg_quality` int(3) NOT NULL DEFAULT '90' COMMENT 'jpeg图片质量',
  `wm_image_name` varchar(255) DEFAULT NULL COMMENT '水印图片的路径以及文件名',
  `wm_image_pos` tinyint(1) NOT NULL DEFAULT '1' COMMENT '水印图片放置的位置',
  `wm_image_transition` int(3) NOT NULL DEFAULT '20' COMMENT '水印图片与原图片的融合度 ',
  `wm_text` text COMMENT '水印文字',
  `wm_text_size` int(3) NOT NULL DEFAULT '20' COMMENT '水印文字大小',
  `wm_text_angle` tinyint(1) NOT NULL DEFAULT '4' COMMENT '水印文字角度',
  `wm_text_pos` tinyint(1) NOT NULL DEFAULT '3' COMMENT '水印文字放置位置',
  `wm_text_font` varchar(50) DEFAULT NULL COMMENT '水印文字的字体',
  `wm_text_color` varchar(7) NOT NULL DEFAULT '#CCCCCC' COMMENT '水印字体的颜色值',
  `wm_is_open` tinyint(1) NOT NULL DEFAULT '0' COMMENT '水印是否开启 0关闭 1开启',
  `store_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='店铺水印图片表';

/*Data for the table `shop_store_watermark` */

/*Table structure for table `shop_store_words` */

DROP TABLE IF EXISTS `shop_store_words`;

CREATE TABLE `shop_store_words` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `keyword` varchar(100) DEFAULT NULL,
  `quanping` varchar(50) CHARACTER SET latin1 DEFAULT NULL COMMENT '全拼',
  `shouzimu` varchar(20) CHARACTER SET latin1 DEFAULT NULL COMMENT '关键词首字母',
  `words_num` int(11) DEFAULT NULL COMMENT '关键词出现次数',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品关键词表';

/*Data for the table `shop_store_words` */

/*Table structure for table `shop_sys_log` */

DROP TABLE IF EXISTS `shop_sys_log`;

CREATE TABLE `shop_sys_log` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `type` char(1) DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) DEFAULT '' COMMENT '日志标题',
  `create_by` varchar(100) DEFAULT NULL COMMENT '创建者',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `remote_addr` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(255) DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) DEFAULT NULL COMMENT '请求URI',
  `method` varchar(5) DEFAULT NULL COMMENT '操作方式',
  `params` text COMMENT '操作提交的数据',
  `exception` text COMMENT '异常信息',
  PRIMARY KEY (`id`),
  KEY `sys_log_create_by` (`create_by`) USING BTREE,
  KEY `sys_log_request_uri` (`request_uri`) USING BTREE,
  KEY `sys_log_type` (`type`) USING BTREE,
  KEY `sys_log_create_date` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志表';

/*Data for the table `shop_sys_log` */

/*Table structure for table `shop_transport` */

DROP TABLE IF EXISTS `shop_transport`;

CREATE TABLE `shop_transport` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `title` varchar(30) NOT NULL COMMENT '运费模板名称',
  `send_tpl_id` bigint(11) DEFAULT NULL,
  `store_id` bigint(11) DEFAULT NULL,
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `is_default` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运费模板';

/*Data for the table `shop_transport` */

/*Table structure for table `shop_transport_extend` */

DROP TABLE IF EXISTS `shop_transport_extend`;

CREATE TABLE `shop_transport_extend` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `type` varchar(5) DEFAULT NULL COMMENT '平邮py 快递kd EMS es',
  `area_id` bigint(11) DEFAULT NULL,
  `top_area_id` bigint(11) DEFAULT NULL,
  `area_name` text COMMENT '地区name组成的串，以，隔开',
  `snum` mediumint(8) unsigned DEFAULT '1' COMMENT '首件数量',
  `sprice` decimal(10,2) DEFAULT '0.00' COMMENT '首件运费',
  `xnum` mediumint(8) unsigned DEFAULT '1' COMMENT '续件数量',
  `xprice` decimal(10,2) DEFAULT '0.00' COMMENT '续件运费',
  `is_default` enum('1','2') DEFAULT '2' COMMENT '是否默认运费1是2否',
  `transport_id` bigint(11) DEFAULT NULL,
  `transport_title` varchar(60) DEFAULT NULL COMMENT '运费模板',
  `is_del` tinyint(4) DEFAULT '0' COMMENT '是否删除0:未删除;1:已删除',
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运费模板扩展表';

/*Data for the table `shop_transport_extend` */

/*Table structure for table `shop_type` */

DROP TABLE IF EXISTS `shop_type`;

CREATE TABLE `shop_type` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `type_name` varchar(100) NOT NULL COMMENT '类型名称',
  `type_sort` int(3) DEFAULT '0' COMMENT '商品类型排序',
  `st_parent_id` bigint(11) DEFAULT NULL,
  `st_idpath` varchar(199) DEFAULT NULL COMMENT '层级path',
  `expen_scale` float DEFAULT NULL COMMENT '费用比例',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品类型表';

/*Data for the table `shop_type` */

/*Table structure for table `shop_type_brand` */

DROP TABLE IF EXISTS `shop_type_brand`;

CREATE TABLE `shop_type_brand` (
  `id` bigint(11) DEFAULT NULL,
  `brand_id` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品类型与品牌对应表';

/*Data for the table `shop_type_brand` */

/*Table structure for table `shop_type_spec` */

DROP TABLE IF EXISTS `shop_type_spec`;

CREATE TABLE `shop_type_spec` (
  `id` bigint(11) DEFAULT NULL,
  `sp_id` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品类型与规格对应表';

/*Data for the table `shop_type_spec` */

/*Table structure for table `shop_upload` */

DROP TABLE IF EXISTS `shop_upload`;

CREATE TABLE `shop_upload` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `file_name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `file_thumb` varchar(100) DEFAULT NULL COMMENT '缩微图片',
  `file_wm` varchar(100) DEFAULT NULL COMMENT '水印图片',
  `file_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `store_id` bigint(11) DEFAULT NULL,
  `upload_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '文件类别，0为无，1为文章图片，默认为0，2为商品切换图片，3为商品内容图片，4为系统文章图片，5为积分礼品切换图片，6为积分礼品内容图片',
  `create_time` bigint(13) DEFAULT NULL COMMENT '添加时间',
  `item_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='上传文件表';

/*Data for the table `shop_upload` */

/*Table structure for table `shop_web` */

DROP TABLE IF EXISTS `shop_web`;

CREATE TABLE `shop_web` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `web_name` varchar(20) DEFAULT '' COMMENT '模块名称',
  `style_name` varchar(20) DEFAULT 'orange' COMMENT '风格名称',
  `web_page` varchar(10) DEFAULT 'index' COMMENT '所在页面(暂时只有index)',
  `update_time` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `web_sort` tinyint(1) unsigned DEFAULT '9' COMMENT '排序',
  `web_show` tinyint(1) unsigned DEFAULT '1' COMMENT '是否显示，0为否，1为是，默认为1',
  `web_html` text COMMENT '模块html代码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='页面模块表';

/*Data for the table `shop_web` */

/*Table structure for table `shop_web_code` */

DROP TABLE IF EXISTS `shop_web_code`;

CREATE TABLE `shop_web_code` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `web_id` bigint(11) DEFAULT NULL,
  `code_type` varchar(10) DEFAULT 'array' COMMENT '数据类型:array,html,json',
  `var_name` varchar(20) NOT NULL COMMENT '变量名称',
  `code_info` text COMMENT '内容数据',
  `show_name` varchar(20) DEFAULT '' COMMENT '楼层名称',
  `is_del` tinyint(1) DEFAULT NULL,
  `create_time` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(13) DEFAULT NULL COMMENT '修改时间',
  `is_show` tinyint(1) DEFAULT NULL COMMENT '是否显示1：显示；0：不显示',
  `sort` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `web_id` (`web_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模块内容表';

/*Data for the table `shop_web_code` */

/*Table structure for table `test_data` */

DROP TABLE IF EXISTS `test_data`;

CREATE TABLE `test_data` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `birthday` bigint(13) DEFAULT NULL COMMENT '加入日期',
  `create_by` int(11) DEFAULT NULL COMMENT '创建者',
  `create_date` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `update_by` int(11) DEFAULT NULL COMMENT '更新者',
  `update_date` bigint(13) DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码生成器测试单表';

/*Data for the table `test_data` */

/*Table structure for table `union_goods` */

DROP TABLE IF EXISTS `union_goods`;

CREATE TABLE `union_goods` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `class_id` bigint(11) DEFAULT NULL,
  `goodsName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品名称',
  `image_url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品图片',
  `skuId` int(11) DEFAULT NULL COMMENT 'skuId',
  `unitPrice` double(10,1) DEFAULT NULL COMMENT '商品单价即京东价（单价为-1表示未查询到改商品单价）',
  `commisionRatioPc` double(10,2) DEFAULT NULL COMMENT 'PC佣金比例',
  `commisionRatioWl` double(10,2) DEFAULT NULL COMMENT '无线佣金比例',
  `shopId` int(11) DEFAULT NULL COMMENT '店铺ID',
  `materialUrl` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品落地页',
  `startDate` bigint(13) DEFAULT NULL COMMENT '推广开始日期',
  `endDate` bigint(13) DEFAULT NULL COMMENT '推广结束日期',
  `source` int(1) DEFAULT NULL COMMENT '商品来源（例如：京东、淘宝）目前值只有"1"代表京东',
  `show_flag` int(1) DEFAULT NULL COMMENT '是否显示（0：不显示；1：显示）',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '创建者',
  `create_date` bigint(13) DEFAULT NULL COMMENT '创建日期',
  `remarks` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `del_flag` char(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '删除标记（0：正常；1：删除；2：审核）',
  `update_by` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '更新者',
  `update_date` bigint(13) DEFAULT NULL COMMENT '更新日期',
  `class_ids` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '分类ids',
  `class_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '分类名称',
  `class_names` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所有分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='联盟商品';

/*Data for the table `union_goods` */

/*Table structure for table `union_goods_class` */

DROP TABLE IF EXISTS `union_goods_class`;

CREATE TABLE `union_goods_class` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '分类名称',
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '图标',
  `show_flag` int(1) DEFAULT NULL COMMENT '是否显示（0：不显示；1：显示）',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `create_by` int(11) DEFAULT NULL COMMENT '创建者',
  `create_date` bigint(13) DEFAULT NULL COMMENT '创建日期',
  `remarks` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '备注',
  `del_flag` int(1) DEFAULT NULL COMMENT '删除标记（0：正常；1：删除；2：审核）',
  `update_by` int(11) DEFAULT NULL COMMENT '更新者',
  `update_date` bigint(13) DEFAULT NULL COMMENT '更新日期',
  `pid` int(11) DEFAULT '0' COMMENT '父id',
  `levels` int(1) DEFAULT '1' COMMENT '层级',
  `idpaths` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'idpaths',
  `names` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '所有分类名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='联盟商品分类';

/*Data for the table `union_goods_class` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
