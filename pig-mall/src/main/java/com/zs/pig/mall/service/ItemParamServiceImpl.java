package com.zs.pig.mall.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zs.pig.common.base.ServiceMybatis;
import com.zs.pig.common.base.model.PigResult;
import com.zs.pig.mall.api.model.TbItemParam;
import com.zs.pig.mall.api.service.ItemParamService;
import com.zs.pig.mall.mapper.TbItemParamMapper;

/**
 * 商品规格参数模板管理
 * <p>Title: ItemParamServiceImpl</p>
 * <p>Description: </p>
 * <p>Company: www.itcast.com</p> 
 * @author	入云龙
 * @date	2015年9月5日下午2:36:57
 * @version 1.0
 */
@Service
public class ItemParamServiceImpl extends
ServiceMybatis<TbItemParam> implements ItemParamService {

	@Autowired
	private TbItemParamMapper itemParamMapper;
	
	@Override
	public PigResult getItemParamByCid(long cid) {
//		TbItemParamExample example = new TbItemParamExample();
//		Criteria criteria = example.createCriteria();
//		criteria.andItemCatIdEqualTo(cid);
//		List<TbItemParam> list = itemParamMapper.selectByExampleWithBLOBs(example);
//		//判断是否查询到结果
//		if (list != null && list.size() > 0) {
//			return PigResult.ok(list.get(0));
//		}
		
		return PigResult.ok();
	}

	@Override
	public PigResult insertItemParam(TbItemParam itemParam) {
		//补全pojo
		itemParam.setCreated(new Date());
		itemParam.setUpdated(new Date());
		//插入到规格参数模板表
		itemParamMapper.insert(itemParam);
		return PigResult.ok();
	}

}
