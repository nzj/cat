package com.zs.pig.gw.portl.beeltFunction;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class StrutilFunction {

	public String subStringTo(String str,int start,int end){
		if(str != null && str.length() > 0){
			return str.substring(start, end);
		}
		return "";
	}
	
	public String subStringLen(String str,int len){
		if(str != null && str.length() > 0 && len < str.length()){
			return str.substring(0,len)+"…";
		}
		return str;
	}
	public List<String> stringToList(String imgs,int count){
		List<String> imagesList=new ArrayList<String>();
		if(imgs!=null){
			String[] images=imgs.split(",");
			for(int i=0;i<images.length;i++){
				imagesList.add(images[i]);
				if(i==count){
					break;
				}
			}
		}
		return imagesList;
	}
	
}
