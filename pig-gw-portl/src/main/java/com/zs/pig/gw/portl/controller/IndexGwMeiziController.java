package com.zs.pig.gw.portl.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.zs.pig.blog.api.model.Blog;
import com.zs.pig.blog.api.service.BlogService;
import com.zs.pig.cms.api.model.CmsArticle;
import com.zs.pig.cms.api.model.CmsCategory;
import com.zs.pig.cms.api.model.Nav;
import com.zs.pig.cms.api.service.CmsArticleService;
import com.zs.pig.cms.api.service.CmsCategoryService;
import com.zs.pig.cms.api.service.CmsImgService;
import com.zs.pig.cms.api.service.NavService;
	/**
	 * 
	 * @author zs 2016-5-5 11:33:51
	 * @Email: 951449465@qq.com
	 * @version 4.0v
	 *	我的blog
	 */
@Controller
@RequestMapping("web/cms")
public class IndexGwMeiziController {

	
	@Resource
	private CmsArticleService CmsArticleService;
	@Resource
	private CmsCategoryService CmsCategoryService;
	@Resource
	private NavService NavService;
	@Resource
	private CmsImgService CmsImgService;
	@Resource
	private BlogService BlogService;
	/**
	 * 请求主页
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/index")
	public ModelAndView index()throws Exception{
		ModelAndView mav=new ModelAndView();
		
		mav.setViewName("gwmeizi/index");
		return mav;
	}
	@RequestMapping("/about")
	public ModelAndView about()throws Exception{
		ModelAndView mav=new ModelAndView();
		
		mav.setViewName("gwmeizi/about");
		return mav;
	}
	@RequestMapping("/index/ajax/bna")
	public String ajaxindex(HttpServletRequest request) {
		try {
			String id = request.getParameter("order");
			if (id != null && !id.equals("")) {
				CmsArticle a=new CmsArticle();
				PageInfo<CmsArticle> artList =null;
				if("reco".equals(id)){
					a.setWeight(1);
					artList = CmsArticleService.selectPage(1, 40, a,"");
				}else if("late".equals(id)){
					artList = CmsArticleService.selectPage(1, 40, a,"createdate desc");
				}else if("hot".equals(id)){
					artList = CmsArticleService.selectPage(1, 40, a,"hits desc");
				}
				
				request.setAttribute("artList", artList.getList());
			}
		} catch (Exception e) {

		}
		return "pc/ajax-index";
	}
	
	@RequestMapping("/indexT")
	public ModelAndView indexT()throws Exception{
		ModelAndView mav=new ModelAndView();
		
		mav.setViewName("pc/indexT");
		return mav;
	}
	
	@RequestMapping("/indexT/ajax/bna")
	public String ajaxindexT(HttpServletRequest request) {
		try {
			String id = request.getParameter("order");
			if (id != null && !id.equals("")) {
				CmsArticle a=new CmsArticle();
				PageInfo<CmsArticle> artList =null;
				if("reco".equals(id)){
					a.setWeight(1);
					artList = CmsArticleService.selectPage(1, 40, a);
				}else if("late".equals(id)){
					artList = CmsArticleService.selectPage(1, 40, a,"createdate desc");
				}else if("hot".equals(id)){
					artList = CmsArticleService.selectPage(1, 40, a,"hits desc");
				}
				
				request.setAttribute("artList", artList.getList());
			}
		} catch (Exception e) {

		}
		return "pc/ajax-indexT";
	}
	/**
	 * 产品详细信息
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/house/{id}")
	public String house(@PathVariable("id") Long id,
			@RequestParam(value = "pageNum",required=false,defaultValue="1")Integer pageNum,
			@RequestParam(value = "pageSize",required=false,defaultValue="31")Integer pageSize,
			@ModelAttribute CmsArticle CmsArticle, Model model)throws Exception{
		
		CmsCategory CmsCategory=new CmsCategory();
		CmsCategory.setIsshow(1);
		CmsCategory.setParentId(id);
		List<CmsCategory> cateList=CmsCategoryService.select(CmsCategory);
		model.addAttribute("cateList",cateList);
//		if(cateList!=null && cateList.size()>0){
//			CmsArticle.setCategoryId(cateList.get(0).getId());
//		}
//		PageInfo<CmsArticle> artList=CmsArticleService.selectPage(pageNum, pageSize, CmsArticle);
//		model.addAttribute("page", artList);
		
		return "pc/house";
	}
	
	
}
